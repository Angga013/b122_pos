package com.xsis.bootcamp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import com.xsis.bootcamp.model.CdModel;
import com.xsis.bootcamp.model.DistributorModel;
import com.xsis.bootcamp.model.GenreModel;
import com.xsis.bootcamp.model.KategoriModel;
import com.xsis.bootcamp.service.CdService;
import com.xsis.bootcamp.service.DistributorService;
import com.xsis.bootcamp.service.GenreService;
import com.xsis.bootcamp.service.KategoriService;

@Controller
public class CdController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private CdService service;
	@Autowired
	private KategoriService katser;
	@Autowired
	private GenreService genser;
	@Autowired
	private DistributorService disService;
	
	@RequestMapping(value="/cd")
	public String index(Model model){
		
		return "cd";
	}
	
	@RequestMapping(value="/cd/list")
	public String list(Model model){
		// membuat object list dari class Cd model
		List<CdModel> items = null;
		
		try {
			// object items diisi data dari method get
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable list kemudian diisi dengan object items
		model.addAttribute("list", items);
		
		return "cd/list";
	}
	
	@RequestMapping(value="/cd/add")
	public String add(Model mod){
		List<KategoriModel> listkat = null;
		List<GenreModel> lisgen = null;
		List<DistributorModel> lisdis = null;
		
		try {
			listkat = katser.get();
			lisgen = genser.get();
			lisdis = disService.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		mod.addAttribute("listkat", listkat);
		mod.addAttribute("listgen", lisgen);
		mod.addAttribute("listDistributor", lisdis);
		return "cd/add";
	}
	
	@RequestMapping(value="/cd/save")
	public String save(Model model, @ModelAttribute CdModel item, HttpServletRequest request){
		String proses = request.getParameter("proses");
		String result ="";
		// proses input ke database
		try {
			if(proses.equals("insert")){
				this.service.insert(item);
			}else if(proses.equals("update")){
				this.service.update(item);
			}else if(proses.equals("delete")){
				this.service.delete(item);
			}
			
			result="berhasil";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result ="gagal";
		}
		model.addAttribute("result",result);
		
		return "cd/save";
	}
	
	@RequestMapping(value="/cd/edit")
	public String edit(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object Cd model
		CdModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable item kemudian diisi dengan object item
		model.addAttribute("item", item);
		return "cd/edit";
	}
	
	@RequestMapping(value="/cd/delete")
	public String delete(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object Cd model
		CdModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "cd/delete";
	}
}
