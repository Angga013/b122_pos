/**
 * 
 */
package com.xsis.bootcamp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import com.fasterxml.jackson.annotation.JsonCreator.Mode;
import com.xsis.bootcamp.model.DistributorModel;
import com.xsis.bootcamp.model.KecamatanModel;
import com.xsis.bootcamp.model.KotaModel;
import com.xsis.bootcamp.model.ProvinsiModel;
import com.xsis.bootcamp.service.DistributorService;
import com.xsis.bootcamp.service.KecamatanService;
import com.xsis.bootcamp.service.KotaService;
import com.xsis.bootcamp.service.ProvinsiService;

/**
 * @author ASUS
 *
 */

@Controller
public class DistributorController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private DistributorService service;
	
	@Autowired
	private KecamatanService kecService;
	
	@Autowired
	private KotaService kotaService;
	
	@Autowired
	private ProvinsiService provService;
	
	@RequestMapping(value="/distributor")
	public String index(Model model){
		return "distributor";
	}
	
	@RequestMapping(value="/distributor/list")
	public String list(Model model){
		// membuat object list dari class Distributor Model
		List<DistributorModel> items = null;
		
		try {
			// object items diisi data dari method get
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view,
		// kita buat variable list kemudian diisi dengan object items
		model.addAttribute("list", items);
		
		return "distributor/list";
	}
	
	@RequestMapping(value="/distributor/add")
	public String add(Model model){
		
		return "distributor/add";
	}
	
	@RequestMapping(value="/distributor/save")
	public String save(Model model, @ModelAttribute DistributorModel item, HttpServletRequest request){
		String proses = request.getParameter("proses");
		String result = "";
		// proses input ke database
		try {
			if(proses.equals("insert")){
				this.service.insert(item);
			}
			else if (proses.equals("update")) {
				this.service.update(item);
			}
			else if (proses.equals("delete")) {
				this.service.delete(item);
			}
			
			result="berhasil";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result="gagal";
		}
		model.addAttribute("result", result);
		
		return "distributor/save";
	}
	
	@RequestMapping(value="/distributor/edit")
	public String edit(Model model, HttpServletRequest request){
		// menangkap parameter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object Distributor model
		DistributorModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view
		// kita buat variable item kemudian diisi dengan object item
		model.addAttribute("item", item);
		return "distributor/edit";
	}
	
	@RequestMapping(value="/distributor/delete")
	public String delete(Model model, HttpServletRequest request){
		// menangkap parameter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object Distributor model
		DistributorModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "distributor/delete";
	}
}
