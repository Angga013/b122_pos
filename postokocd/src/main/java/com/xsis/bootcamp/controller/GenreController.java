package com.xsis.bootcamp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import com.xsis.bootcamp.model.GenreModel;
import com.xsis.bootcamp.model.KategoriModel;
import com.xsis.bootcamp.service.GenreService;
import com.xsis.bootcamp.service.KategoriService;

@Controller
public class GenreController {
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private GenreService service;

	@Autowired
	private KategoriService kservice;

	@RequestMapping(value = "/genre")
	public String index(Model model) {

		return "genre";
	}

	@RequestMapping(value = "/genre/list")
	public String list(Model model) {
		// membuat object list dari class genre model
		List<GenreModel> items = null;

		try {
			// object items diisi data dari method get
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		// datanya kita kirim ke view,
		// kita buat variable list kemudian diisi dengan object items
		model.addAttribute("list", items);

		return "genre/list";
	}

	@RequestMapping(value = "/genre/add")
	public String add(Model model) {
		List<KategoriModel> listkat = null;

		try {
			listkat = kservice.get();
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", listkat);
		return "genre/add";
	}

	@RequestMapping(value = "/genre/save")
	public String save(Model model, @ModelAttribute GenreModel item, HttpServletRequest request) {
		String proses = request.getParameter("proses");
		String result = "";
		// proses input ke database
		try {
			if (proses.equals("insert")) {
				this.service.insert(item);
			} else if (proses.equals("update")) {
				this.service.update(item);
			} else if (proses.equals("delete")) {
				this.service.delete(item);
			}

			result = "berhasil";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "gagal";
		}
		model.addAttribute("result", result);

		return "genre/save";
	}

	@RequestMapping(value = "/genre/edit")
	public String edit(Model model, HttpServletRequest request) {
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));

		// siapkan object genre model
		GenreModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		// datanya kita kirim ke view,
		// kita buat variable item kemudian diisi dengan object item
		model.addAttribute("item", item);
		List<KategoriModel> listkat = null;

		try {
			listkat = kservice.get();
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", listkat);
		return "genre/edit";
	}

	@RequestMapping(value = "/genre/delete")
	public String delete(Model model, HttpServletRequest request) {
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));

		// siapkan object genre model
		GenreModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "genre/delete";
	}
}
