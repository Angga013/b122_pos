package com.xsis.bootcamp.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.bootcamp.model.CdModel;
import com.xsis.bootcamp.model.GenreModel;
import com.xsis.bootcamp.model.KategoriModel;
import com.xsis.bootcamp.model.PelangganModel;
import com.xsis.bootcamp.model.PenjualanDetailModel;
import com.xsis.bootcamp.model.PenjualanModel;
import com.xsis.bootcamp.service.CdService;
import com.xsis.bootcamp.service.GenreService;
import com.xsis.bootcamp.service.KategoriService;
import com.xsis.bootcamp.service.PelangganService;
import com.xsis.bootcamp.service.PenjualanDetailService;
import com.xsis.bootcamp.service.PenjualanService;

@Controller
public class TransaksiController {
	@Autowired
	private PenjualanService penService;
	
	@Autowired 
	private PenjualanDetailService detailService;
	
	@Autowired
	private PelangganService pelService;
	
	@Autowired
	private CdService cservice;
	
	@Autowired
	private PelangganService pservice;
	
	Log log = LogFactory.getLog(getClass());
	
	@RequestMapping(value="/transaksi")
	public String Home(Model model){
		return "transaksi";
	}
	
	@RequestMapping(value="/transaksi/add")
	public String add(){
		return "transaksi/add";
	}
	
	@RequestMapping(value="/transaksi/cdSearch")
	public String pasienSearch(Model model, HttpServletRequest request){
		// membuat object list dari class Mahasiswa model
				List<CdModel> items = null;
				
				try {
					// object items diisi data dari method get
					items = this.cservice.get();
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
				
				// datanya kita kirim ke view, 
				// kita buat variable list kemudian diisi dengan object items
				model.addAttribute("list", items);
				
		return "transaksi/listbarang";
	}
	
	@RequestMapping(value = "/transaksi/list")
	public String list(Model model) {
		List<PenjualanModel> items = null;

		try {
			items = this.penService.get();
		} catch (Exception e) {
			e.printStackTrace();
		}

		model.addAttribute("list", items);
		return "transaksi/list";
	}
	
	@RequestMapping(value="/transaksi/listPelanggan")
	public String listpelanggan(Model model){
		// membuat object list dari class Pelanggan Model
		List<PelangganModel> items = null;
		
		try {
			// object items diisi data dari method get
			items = this.pservice.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view,
		// kita buat variable list kemudian diisi dengan object items
		model.addAttribute("list", items);
		
		return "transaksi/listPelanggan";
	}
	@RequestMapping(value = "/transaksi/save", method = RequestMethod.POST)
	private String save(Model model, @ModelAttribute PenjualanModel item, HttpServletRequest request) {
		String proses = request.getParameter("proses");
		Integer kasir = Integer.parseInt(request.getParameter("kasir"));
		String result = "";
		
        
		try {
			if (proses.equals("insert")) {
				item.setUserId(kasir);
				this.penService.insert(item);
			} else if (proses.equals("update")) {
				this.penService.update(item);
			} else if (proses.equals("delete")) {
				this.penService.delete(item);
			}
			result = "berhasil";
		} catch (Exception e) {
			log.error("Error @ save pembelian: " + e.getMessage(), e);
			result = "gagal";
		}

		model.addAttribute("result", result);
		return "pembelian/save";
	}
	
	@RequestMapping(value = "transaksi/detailTransaksi")
	public String pembelianDetail(Model model, HttpServletRequest request){
		PenjualanModel penjualanModel = new PenjualanModel();
		Integer id = Integer.parseInt(request.getParameter("id"));
		List<PenjualanDetailModel> items = null;
		
		try {
			penjualanModel = penService.getById(id);
			items = penjualanModel.getDetailPenjualans();
		} catch (Exception e) {
			log.error("Error @ Load Detail Pembelian: "+e.getMessage(), e);
		}
		
		System.out.println("model-nomor faktur : "+items.size());
		
		model.addAttribute("list", items);
		model.addAttribute("item", penjualanModel);
		return "transaksi/listDetailTransaksi";
	}
}
