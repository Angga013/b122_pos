/**
 * 
 */
package com.xsis.bootcamp.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import com.fasterxml.jackson.annotation.JsonCreator.Mode;
import com.xsis.bootcamp.model.PelangganModel;
import com.xsis.bootcamp.model.KecamatanModel;
import com.xsis.bootcamp.model.KotaModel;
import com.xsis.bootcamp.model.ProvinsiModel;
import com.xsis.bootcamp.service.PelangganService;
import com.xsis.bootcamp.service.KecamatanService;
import com.xsis.bootcamp.service.KotaService;
import com.xsis.bootcamp.service.PelangganService;
import com.xsis.bootcamp.service.ProvinsiService;



@Controller
public class PelangganController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private PelangganService service;
	
	@Autowired
	private KecamatanService kecService;
	
	@Autowired
	private KotaService kotaService;
	
	@Autowired
	private ProvinsiService provService;
	
	@RequestMapping(value="/pelanggan")
	public String index(Model model){
		return "pelanggan";
	}
	
	@RequestMapping(value="/pelanggan/list")
	public String list(Model model){
		// membuat object list dari class Pelanggan Model
		List<PelangganModel> items = null;
		
		try {
			// object items diisi data dari method get
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view,
		// kita buat variable list kemudian diisi dengan object items
		model.addAttribute("list", items);
		
		return "pelanggan/list";
	}
	
	@RequestMapping(value="/pelanggan/add")
	public String add(Model model){
		
		return "pelanggan/add";
	}
	
	@RequestMapping(value="/pelanggan/save")
	public String save(Model model, @ModelAttribute PelangganModel item, HttpServletRequest request){
		String proses = request.getParameter("proses");
		String result = "";
		// proses input ke database
		try {
			if(proses.equals("insert")){
				this.service.insert(item);
			}
			else if (proses.equals("update")) {
				this.service.update(item);
			}
			else if (proses.equals("delete")) {
				this.service.delete(item);
			}
			
			result="berhasil";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result="gagal";
		}
		model.addAttribute("result", result);
		
		return "pelanggan/save";
	}
	
	@RequestMapping(value="/pelanggan/edit")
	public String edit(Model model, HttpServletRequest request){
		// menangkap parameter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object Pelanggan model
		PelangganModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view
		// kita buat variable item kemudian diisi dengan object item
		model.addAttribute("item", item);
		return "pelanggan/edit";
	}
	
	@RequestMapping(value="/pelanggan/delete")
	public String delete(Model model, HttpServletRequest request){
		// menangkap parameter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object Pelanggan model
		PelangganModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "pelanggan/delete";
	}
}
