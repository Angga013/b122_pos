/**
 * 
 */
package com.xsis.bootcamp.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.CdModel;
import com.xsis.bootcamp.model.DistributorModel;
import com.xsis.bootcamp.model.PembelianDetailModel;
import com.xsis.bootcamp.model.PembelianModel;
import com.xsis.bootcamp.service.CdService;
import com.xsis.bootcamp.service.DistributorService;
import com.xsis.bootcamp.service.PembelianService;

/**
 * @author ASUS
 *
 */

@Controller
@RequestMapping(value="/pembelian")
public class PembelianController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private PembelianService beliService;
	
	@Autowired
	private CdService cdService;
	
	@Autowired
	private DistributorService distriService;
	
	@RequestMapping
	public String index(Model model){
		return "pembelian";
	}
	
	@RequestMapping(value="/list")
	public String list(Model model){
		// membuat object list dari class Pembelian model
		List<PembelianModel> items = null;
		
		try {
			// object items diisi dari method get
			items = this.beliService.get();
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view
		// kita buat variabel list kemudian diisi dengan object items
		model.addAttribute("list", items);
		return "pembelian/list";
	}
	
	@RequestMapping(value="/listByTanggalPembelian")
	public String listByTanggalPembelian(Model model, HttpServletRequest request){
		Date date = null;
		List<PembelianModel> items = null;
		
		try {
			date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("tanggalPembelian"));
			items = this.beliService.getByTanggalPembelian(date);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("list", items);
		
		return "pembelian/listByTanggalPembelian";
	}
	
	@RequestMapping(value="/listDistributor")
	public String listDistributor(Model model){
		List<DistributorModel> items = null;
		
		try {
			items = this.distriService.get();
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("list", items);
		
		return "pembelian/listDistributor";
	}
	
	@RequestMapping(value="/listCd")
	public String listCd(Model model){
		List<CdModel> items = null;
		
		try {
			items = this.cdService.get();
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("list", items);
		return "pembelian/listCd";
	}
	
	@RequestMapping(value="/detailPembelian")
	public String pembelianDetail(Model model, HttpServletRequest request){
		PembelianModel pembelianModel = new PembelianModel();
		Integer id = Integer.parseInt(request.getParameter("id"));
		
		try {
			pembelianModel = beliService.getById(id);
		} catch (Exception e) {
			log.error("Error di Load Detail Pembelian: " + e.getMessage(), e);
		}
		
		List<PembelianDetailModel> items = pembelianModel.getDetailPembelians();
		
		model.addAttribute("list", items);
		model.addAttribute("item", pembelianModel);
		return "pembelian/detailPembelian";
	}
	
	@RequestMapping(value="/add")
	public String add(){
		return "pembelian/add";
	}
	
	@RequestMapping(value="/save")
	public String save(Model model, @ModelAttribute PembelianModel item, HttpServletRequest request){
		String proses = request.getParameter("proses");
		String result = "";
		Date date = null;
		
		try {
			if (proses.equals("insert")) {
				date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("tanggalPembelianStr"));
				item.setTanggalPembelian(date);
				this.beliService.insert(item);
			}else if (proses.equals("update")) {
				date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("tanggalPembelianStr"));
				item.setTanggalPembelian(date);
				this.beliService.update(item);
			}else if (proses.equals("delete")) {
				this.beliService.delete(item);
			}
			result="berhasil";
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error di save pembelian: " + e.getMessage(), e);
			result="gagal";
		}
		model.addAttribute("result", result);
		
		return "pembelian/save";
	}
	
	@RequestMapping(value="/edit")
	public String edit(Model model, HttpServletRequest request){
		
		Integer id = Integer.parseInt(request.getParameter("id"));
		
		PembelianModel item = null;
		
		try {
			item = this.beliService.getById(id);
		} catch (Exception e) {
			log.error("Error tarik data edit: " + e.getMessage(), e);
		}
		
		model.addAttribute("item", item);
		
		return "/pembelian/edit";
	}
	
	@RequestMapping(value="/delete")
	public String delete(Model model, HttpServletRequest request){
		Integer id = Integer.parseInt(request.getParameter("id"));
		PembelianModel item = null;
		
		try {
			item = this.beliService.getById(id);
		} catch (Exception e) {
			log.error("Error di delete: "+ e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "pembelian/delete";
	}
}
