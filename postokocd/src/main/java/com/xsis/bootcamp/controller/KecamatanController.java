package com.xsis.bootcamp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.KecamatanModel;
import com.xsis.bootcamp.service.KecamatanService;

@Controller
public class KecamatanController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private KecamatanService service;
	
	@RequestMapping(value="/kecamatan")
	public String index(Model model){
		
		return "kecamatan";
	}
	
	@RequestMapping(value="/kecamatan/list")
	public String list(Model model){
		// membuat object list dari class Kecamatan model
		List<KecamatanModel> items = null;
		
		try {
			// object items diisi data dari method get
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable list kemudian diisi dengan object items
		model.addAttribute("list", items);
		
		return "kecamatan/list";
	}
	
	@RequestMapping(value="/kecamatan/listByKota")
	public String listByKota(Model model, HttpServletRequest request){
		int id = Integer.parseInt(request.getParameter("id"));
		// membuat object list dari class Kecamatan model
		List<KecamatanModel> items = null;
		
		try {
			// object items diisi data dari method get
			items = this.service.getByKotaId(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable list kemudian diisi dengan object items
		model.addAttribute("list", items);
		
		return "kecamatan/listByKota";
	}
	
	@RequestMapping(value="/kecamatan/add")
	public String add(){
		return "kecamatan/add";
	}
	
	@RequestMapping(value="/kecamatan/save")
	public String save(Model model, @ModelAttribute KecamatanModel item, HttpServletRequest request){
		String proses = request.getParameter("proses");
		String result ="";
		// proses input ke database
		try {
			if(proses.equals("insert")){
				this.service.insert(item);
			}else if(proses.equals("update")){
				this.service.update(item);
			}else if(proses.equals("delete")){
				this.service.delete(item);
			}
			
			result="berhasil";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result ="gagal";
		}
		model.addAttribute("result",result);
		
		return "kecamatan/save";
	}
	
	@RequestMapping(value="/kecamatan/edit")
	public String edit(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object Kecamatan model
		KecamatanModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable item kemudian diisi dengan object item
		model.addAttribute("item", item);
		return "kecamatan/edit";
	}
	
	@RequestMapping(value="/kecamatan/delete")
	public String delete(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		int id = Integer.parseInt(request.getParameter("id"));
		
		// siapkan object Kecamatan model
		KecamatanModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "kecamatan/delete";
	}
}
