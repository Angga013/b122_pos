package com.xsis.bootcamp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;import com.fasterxml.jackson.annotation.JsonCreator.Mode;
import com.xsis.bootcamp.model.CdModel;
import com.xsis.bootcamp.model.DistributorModel;
import com.xsis.bootcamp.model.KecamatanModel;
import com.xsis.bootcamp.model.KotaModel;
import com.xsis.bootcamp.model.ProvinsiModel;
import com.xsis.bootcamp.service.CdService;
import com.xsis.bootcamp.service.DistributorService;
import com.xsis.bootcamp.service.KecamatanService;
import com.xsis.bootcamp.service.KotaService;
import com.xsis.bootcamp.service.ProvinsiService;

@Controller
public class AjaxController {
	
	private Log log = LogFactory.getLog(this.getClass()); 
	
	@Autowired
	private ProvinsiService provService;
	
	@Autowired
	private KotaService kotaService;
	
	@Autowired
	private KecamatanService kecService;
	
	@Autowired
	private DistributorService distriService;
	
	@Autowired
	private CdService cdService;
	
	@RequestMapping(value="/ajax/getProvinsi")
	public String getProvinsi(Model model){
		List<ProvinsiModel> result = null;
		
		try {
			result = provService.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("result", result);
		
		return "ajax/getProvinsi";		
	}
	
	@RequestMapping(value="/ajax/getKota")
	public String getKota(Model model, HttpServletRequest request){
		List<KotaModel> result = null;
		
		int id = Integer.parseInt(request.getParameter("id"));
		
		try {
			result = kotaService.getByProvinsiId(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("result", result);
		
		return "ajax/getKota";		
	}

	@RequestMapping(value="/ajax/getKecamatan")
	public String getKecamatan(Model model, HttpServletRequest request){
		List<KecamatanModel> result = null;
		
		int id = Integer.parseInt(request.getParameter("id"));
		
		try {
			result = kecService.getByKotaId(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("result", result);
		
		return "ajax/getKecamatan";		
	}
	
	@RequestMapping(value="/ajax/getDistributor")
	public String getDistributor(Model model){
		List<DistributorModel> result = null;
		
		try {
			result = this.distriService.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("result", result);
		return "ajax/getDistributor";
	}
	
	@RequestMapping(value="/ajax/getCd")
	public String getCd(Model model){
		List<CdModel> result = null;
		
		try {
			result = this.cdService.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("result", result);
		return "ajax/getCd";
	}
	
}
