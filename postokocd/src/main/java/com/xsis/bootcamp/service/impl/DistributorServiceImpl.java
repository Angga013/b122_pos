/**
 * 
 */
package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.DistributorDao;
import com.xsis.bootcamp.model.DistributorModel;
import com.xsis.bootcamp.service.DistributorService;

/**
 * @author ASUS
 *
 */

@Service
@Transactional
public class DistributorServiceImpl implements DistributorService {
	
	@Autowired
	private DistributorDao dao;
	
	@Override
	public List<DistributorModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(DistributorModel model) throws Exception {
		this.dao.insert(model);
	}

	@Override
	public DistributorModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(DistributorModel model) throws Exception {
		this.dao.update(model);
	}

	@Override
	public void delete(DistributorModel model) throws Exception {
		this.dao.delete(model);
	}

}
