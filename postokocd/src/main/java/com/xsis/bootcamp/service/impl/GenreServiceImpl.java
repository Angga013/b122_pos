package com.xsis.bootcamp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xsis.bootcamp.dao.GenreDao;
import com.xsis.bootcamp.model.GenreModel;
import com.xsis.bootcamp.service.GenreService;

@Service
@Transactional
public class GenreServiceImpl implements GenreService {
	@Autowired
	private GenreDao dao;
	
	@Override
	public List<GenreModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(GenreModel model) throws Exception {
		this.dao.insert(model);
	}

	@Override
	public GenreModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(GenreModel model) throws Exception {
		this.dao.update(model);
	}

	@Override
	public void delete(GenreModel model) throws Exception {
		this.dao.delete(model);		
	}

}
