package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xsis.bootcamp.dao.CdDao;
import com.xsis.bootcamp.dao.StokCdDao;
import com.xsis.bootcamp.model.StokCdModel;
import com.xsis.bootcamp.service.CdService;
import com.xsis.bootcamp.service.StokCdService;

@Service
@Transactional
public class StokCdServiceImpl implements StokCdService {
	@Autowired
	private StokCdDao dao;
	
	@Override
	public List<StokCdModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(StokCdModel model) throws Exception {
		this.dao.insert(model);
	}

	@Override
	public StokCdModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(StokCdModel model) throws Exception {
		this.dao.update(model);
	}

	@Override
	public void delete(StokCdModel model) throws Exception {
		this.dao.delete(model);		
	}

}
