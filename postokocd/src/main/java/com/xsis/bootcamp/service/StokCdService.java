package com.xsis.bootcamp.service;

import java.util.List;
import com.xsis.bootcamp.model.StokCdModel;

public interface StokCdService {
	public List<StokCdModel> get() throws Exception;
	public void insert(StokCdModel model) throws Exception;
	public StokCdModel getById(int id) throws Exception;
	public void update(StokCdModel model) throws Exception;
	public void delete(StokCdModel model) throws Exception;
}
