package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.CdDao;
import com.xsis.bootcamp.model.CdModel;
import com.xsis.bootcamp.service.CdService;

@Service
@Transactional
public class CdServiceImpl implements CdService {
	@Autowired
	private CdDao dao;
	
	@Override
	public List<CdModel> get() throws Exception {
		return this.dao.get();
	}
	
	@Override
	public List<CdModel> getByKategoriId(int id) throws Exception {
		return this.dao.getByKategoriId(id);
	}
	
	@Override
	public List<CdModel> getByGenreId(int id) throws Exception {
		return this.dao.getByKategoriId(id);
	}
	
	@Override
	public List<CdModel> getByDistributorId(int id) throws Exception {
		return this.dao.getByKategoriId(id);
	}

	@Override
	public void insert(CdModel model) throws Exception {
		Integer hargaJual = model.getHargaBeli()+10000;
		model.setHarga(hargaJual);
		this.dao.insert(model);
	}

	@Override
	public CdModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(CdModel model) throws Exception {
		this.dao.update(model);
	}

	@Override
	public void delete(CdModel model) throws Exception {
		this.dao.delete(model);		
	}

}
