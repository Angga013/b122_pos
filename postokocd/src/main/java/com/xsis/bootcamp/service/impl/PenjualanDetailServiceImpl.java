package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.xsis.bootcamp.dao.PenjualanDetailDao;
import com.xsis.bootcamp.model.PenjualanDetailModel;
import com.xsis.bootcamp.service.PenjualanDetailService;

@Service
@Transactional
public class PenjualanDetailServiceImpl implements PenjualanDetailService {
	@Autowired
	private PenjualanDetailDao dao;
	
	@Override
	public List<PenjualanDetailModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(PenjualanDetailModel model) throws Exception {
		this.dao.insert(model);
	}

	@Override
	public List<PenjualanDetailModel> getByPenjualanId(Integer id) throws Exception {
		return this.dao.getByPenjualanId(id);
	}

	@Override
	public void update(PenjualanDetailModel model) throws Exception {
		this.dao.update(model);
	}

	@Override
	public void delete(PenjualanDetailModel model) throws Exception {
		this.dao.delete(model);	
		
	}
	
}
