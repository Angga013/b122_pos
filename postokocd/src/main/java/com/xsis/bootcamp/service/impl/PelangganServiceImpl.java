package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.PelangganDao;
import com.xsis.bootcamp.model.PelangganModel;
import com.xsis.bootcamp.service.PelangganService;

@Service
@Transactional
public class PelangganServiceImpl implements PelangganService {
	@Autowired
	private PelangganDao dao;
	
	@Override
	public List<PelangganModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(PelangganModel model) throws Exception {
		this.dao.insert(model);
	}

	@Override
	public PelangganModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(PelangganModel model) throws Exception {
		this.dao.update(model);
	}

	@Override
	public void delete(PelangganModel model) throws Exception {
		this.dao.delete(model);		
	}

}
