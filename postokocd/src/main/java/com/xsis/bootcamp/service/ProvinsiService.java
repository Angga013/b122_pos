package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.ProvinsiModel;

public interface ProvinsiService {
	public List<ProvinsiModel> get() throws Exception;
	public void insert(ProvinsiModel model) throws Exception;
	public ProvinsiModel getById(int id) throws Exception;
	public void update(ProvinsiModel model) throws Exception;
	public void delete(ProvinsiModel model) throws Exception;
}
