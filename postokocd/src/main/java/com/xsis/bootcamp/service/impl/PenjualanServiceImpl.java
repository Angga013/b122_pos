package com.xsis.bootcamp.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.CdDao;
import com.xsis.bootcamp.dao.PenjualanDao;
import com.xsis.bootcamp.dao.PenjualanDetailDao;
import com.xsis.bootcamp.dao.StokCdDao;
import com.xsis.bootcamp.model.CdModel;
import com.xsis.bootcamp.model.PenjualanDetailModel;
import com.xsis.bootcamp.model.PenjualanModel;
import com.xsis.bootcamp.model.StokCdModel;
import com.xsis.bootcamp.service.CdService;
import com.xsis.bootcamp.service.PenjualanService;

@Service
@Transactional
public class PenjualanServiceImpl implements PenjualanService {
	@Autowired
	private PenjualanDao dao;
	
	@Autowired
	private CdDao cdDao;
	
	@Autowired
	private PenjualanDetailDao pdDao;
	
	@Autowired
	private StokCdDao scd;
	
	@Override
	public List<PenjualanModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(PenjualanModel model) throws Exception {
		Date date = new Date();
		CdModel cdmod = new CdModel();
		SimpleDateFormat ft = new SimpleDateFormat("yyMMddhhmmss");
        String datetime = ft.format(date);
        Integer total= 0;
        Integer grandTotal = 0;
        Integer stok = 0;
        
        model.setNoFaktur("F"+datetime);
		model.setTglTransaksi(date);
		this.dao.insert(model);
		
		for (PenjualanDetailModel detail : model.getDetailPenjualans()) {
			PenjualanDetailModel item = new PenjualanDetailModel();			
			item.setPenjualanId(model.getId());			
			item.setCdId(detail.getCdId());
			item.setJumlah(detail.getJumlah());
			cdmod=cdDao.getById(detail.getCdId());
			
			StokCdModel sCdModel = cdmod.getStokModel();
			
			Integer stokAwal = sCdModel.getJumlahStok();
			Integer stokKurang = detail.getJumlah();
			Integer stokAkhir = stokAwal-stokKurang;
			sCdModel.setJumlahStok(stokAkhir);
			this.scd.update(sCdModel);
			
			total = cdmod.getHarga()*detail.getJumlah();
			item.setSubTotal(total);
			grandTotal = grandTotal+total; 
			this.pdDao.insert(item);
			}
		model.setTotal(grandTotal);
		dao.update(model);
	}

	@Override
	public PenjualanModel getById(Integer id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public List<PenjualanModel> getByTglTransaksi(Date tglTransaksi) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<PenjualanModel> getByUserId(Integer id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(PenjualanModel model) throws Exception {
		this.dao.update(model);
		
	}

	@Override
	public void delete(PenjualanModel model) throws Exception {
		this.dao.delete(model);	
		
	}
	
	

}
