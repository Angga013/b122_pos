/**
 * 
 */
package com.xsis.bootcamp.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.CdDao;
import com.xsis.bootcamp.dao.PembelianDao;
import com.xsis.bootcamp.dao.StokCdDao;
import com.xsis.bootcamp.model.CdModel;
import com.xsis.bootcamp.model.PembelianDetailModel;
import com.xsis.bootcamp.model.PembelianModel;
import com.xsis.bootcamp.model.StokCdModel;
import com.xsis.bootcamp.service.PembelianService;

/**
 * @author ASUS
 *
 */

@Service
@Transactional
public class PembelianServiceImpl implements PembelianService {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private PembelianDao dao;	
	
	@Autowired
	private CdDao cdDao;
	
	@Autowired
	private StokCdDao sCd;
	
	@Override
	public List<PembelianModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public List<PembelianModel> getByTanggalPembelian(Date tanggalPembelian) throws Exception {
		return this.dao.getByTanggalPembelian(tanggalPembelian);
	}

	@Override
	public List<PembelianModel> getByDistributorId(Integer id) throws Exception {
		return this.dao.getByDistributorId(id);
	}

	@Override
	public void insert(PembelianModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		CdModel cdModel = new CdModel();
		
		Integer subTotal = 0;
		Integer grandTotal = 0;
		Integer stok = 0;
		this.dao.insert(model);
		for (PembelianDetailModel detail : model.getDetailPembelians()) {
			PembelianDetailModel item = new PembelianDetailModel();
			
			item.setCdId(detail.getCdId());
			
			item.setPembelianId(detail.getPembelianId());
			
			cdModel = cdDao.getById(detail.getCdId());
			
			StokCdModel sCd = cdModel.getStokModel();
			Integer stokAwal = sCd.getJumlahStok();
			Integer stokTambah = detail.getJumlahBeli();
			Integer stokAkhir = stokAwal+stokTambah;
			sCd.setJumlahStok(stokAkhir);
			this.sCd.update(sCd);
			
			item.setHargaSatuan(cdModel.getHargaBeli());
			item.setJumlahBeli(detail.getJumlahBeli());	
			
			subTotal = cdModel.getHargaBeli() * detail.getJumlahBeli();
			item.setSubTotal(subTotal);
			grandTotal = grandTotal+subTotal;
			session.save(item);
		}
		model.setTotal(grandTotal);
		
	}

	@Override
	public PembelianModel getById(Integer id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(PembelianModel model) throws Exception {
		this.dao.update(model);
	}

	@Override
	public void delete(PembelianModel model) throws Exception {
		this.dao.delete(model);
	}

}
