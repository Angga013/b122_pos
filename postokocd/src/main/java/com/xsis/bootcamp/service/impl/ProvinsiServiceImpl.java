package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.ProvinsiDao;
import com.xsis.bootcamp.model.ProvinsiModel;
import com.xsis.bootcamp.service.ProvinsiService;

@Service
@Transactional
public class ProvinsiServiceImpl implements ProvinsiService {
	@Autowired
	private ProvinsiDao dao;
	
	@Override
	public List<ProvinsiModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(ProvinsiModel model) throws Exception {
		this.dao.insert(model);
	}

	@Override
	public ProvinsiModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(ProvinsiModel model) throws Exception {
		this.dao.update(model);
	}

	@Override
	public void delete(ProvinsiModel model) throws Exception {
		this.dao.delete(model);		
	}

}
