package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.UserDao;
import com.xsis.bootcamp.model.UserModel;
import com.xsis.bootcamp.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao dao;
	
	@Override
	public List<UserModel> get() throws Exception {
		return this.dao.get();
	}
	
	@Override
	public List<UserModel> getByRoleId(int id) throws Exception {
		return this.dao.getByRoleId(id);
	}

	@Override
	public void insert(UserModel model) throws Exception {
		this.dao.insert(model);
	}

	@Override
	public UserModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(UserModel model) throws Exception {
		this.dao.update(model);
	}

	@Override
	public void delete(UserModel model) throws Exception {
		this.dao.delete(model);		
	}

}
