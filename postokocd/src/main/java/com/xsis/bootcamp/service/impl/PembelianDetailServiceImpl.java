/**
 * 
 */
package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.PembelianDetailDao;
import com.xsis.bootcamp.model.PembelianDetailModel;
import com.xsis.bootcamp.service.PembelianDetailService;

/**
 * @author ASUS
 *
 */

@Service
@Transactional
public class PembelianDetailServiceImpl implements PembelianDetailService {
	@Autowired
	private PembelianDetailDao dao;
	
	
	@Override
	public List<PembelianDetailModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public List<PembelianDetailModel> getByPembelianId(Integer id) throws Exception {
		return this.dao.getByPembelianId(id);
	}

	@Override
	public List<PembelianDetailModel> getByCdId(Integer id) throws Exception {
		return this.dao.getByCdId(id);
	}

	@Override
	public void insert(PembelianDetailModel model) throws Exception {
		this.dao.insert(model);
	}

	@Override
	public PembelianDetailModel getById(Integer id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(PembelianDetailModel model) throws Exception {
		this.dao.update(model);
	}

	@Override
	public void delete(PembelianDetailModel model) throws Exception {
		this.dao.delete(model);
	}

}
