/**
 * 
 */
package com.xsis.bootcamp.service;

import java.util.Date;
import java.util.List;

import com.xsis.bootcamp.model.PembelianModel;

/**
 * @author ASUS
 *
 */
public interface PembelianService {
	public List<PembelianModel> get() throws Exception;
	public List<PembelianModel> getByTanggalPembelian(Date tanggalPembelian) throws Exception;
	public List<PembelianModel> getByDistributorId(Integer id) throws Exception;
	public void insert(PembelianModel model) throws Exception;
	public PembelianModel getById(Integer id) throws Exception;
	public void update(PembelianModel model) throws Exception;
	public void delete(PembelianModel model) throws Exception;
}
