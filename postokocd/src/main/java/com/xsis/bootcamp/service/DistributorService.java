/**
 * 
 */
package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.DistributorModel;

/**
 * @author ASUS
 *
 */
public interface DistributorService {
	public List<DistributorModel> get() throws Exception;
	public void insert(DistributorModel model) throws Exception;
	public DistributorModel getById(int id) throws Exception;
	public void update(DistributorModel model) throws Exception;
	public void delete(DistributorModel model) throws Exception;
}
