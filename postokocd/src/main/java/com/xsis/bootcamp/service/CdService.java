package com.xsis.bootcamp.service;

import java.util.List;
import com.xsis.bootcamp.model.CdModel;

public interface CdService {
	public List<CdModel> get() throws Exception;
	public List<CdModel> getByKategoriId(int id) throws Exception;
	public List<CdModel> getByGenreId(int id) throws Exception;
	public List<CdModel> getByDistributorId(int id) throws Exception;
	public void insert(CdModel model) throws Exception;
	public CdModel getById(int id) throws Exception;
	public void update(CdModel model) throws Exception;
	public void delete(CdModel model) throws Exception;
}
