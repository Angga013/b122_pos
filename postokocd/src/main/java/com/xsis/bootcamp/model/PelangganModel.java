package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="PELANGGAN")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id" )
public class PelangganModel {
	private Integer id;
	private String namaPelanggan;
	private String alamat;
	private Integer kecamatanId;
	private Integer kotaId;
	private Integer provinsiId;
	private String noTelp;
	private String email;
	
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="PELANGGAN")
	@TableGenerator(name="PELANGGAN",table="SEQUENCE", pkColumnName="SEQUENCE_ID", pkColumnValue="PELANGGAN",valueColumnName="SEQUENCE_VALUE", allocationSize=1,initialValue=1)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="NAMA_PELANGGAN")
	public String getNamaPelanggan() {
		return namaPelanggan;
	}
	public void setNamaPelanggan(String namaPelanggan) {
		this.namaPelanggan = namaPelanggan;
	}
	@Column(name="ALAMAT")
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	@Column(name="KECAMATAN_ID")
	public Integer getKecamatanId() {
		return kecamatanId;
	}
	public void setKecamatanId(Integer kecamatanId) {
		this.kecamatanId = kecamatanId;
	}
	@Column(name="KOTA_ID")
	public Integer getKotaId() {
		return kotaId;
	}
	public void setKotaId(Integer kotaId) {
		this.kotaId = kotaId;
	}
	@Column(name="PROVINSI_ID")
	public Integer getProvinsiId() {
		return provinsiId;
	}
	public void setProvinsiId(Integer provinsiId) {
		this.provinsiId = provinsiId;
	}
	@Column(name="NOMOR_TELEPON")
	public String getNoTelp() {
		return noTelp;
	}
	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}
	@Column(name="EMAIL")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
		
}
