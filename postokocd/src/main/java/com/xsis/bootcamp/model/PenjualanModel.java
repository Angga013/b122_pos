package com.xsis.bootcamp.model;


import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.criteria.Fetch;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="PENJUALAN")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
public class PenjualanModel {
	private Integer id;
	private String NoFaktur;
	private Date tglTransaksi;
	private Integer pelangganId;
	private Integer total;
	private Integer userId;
	
	private List<PenjualanDetailModel> detailPenjualans;
	
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="PENJUALAN")
	@TableGenerator(name="PENJUALAN",table="SEQUENCE", pkColumnName="SEQUENCE_ID", pkColumnValue="TRANSAKSI", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="NOMOR_FAKTUR")
	public String getNoFaktur() {
		return NoFaktur;
	}
	public void setNoFaktur(String noFaktur) {
		NoFaktur = noFaktur;
	}
	@Column(name="TANGGAL_TRANSAKSI")
	@Temporal(TemporalType.DATE)
	public Date getTglTransaksi() {
		return tglTransaksi;
	}
	public void setTglTransaksi(Date tglTransaksi) {
		this.tglTransaksi = tglTransaksi;
	}
	@Column(name="PELANGGAN_ID")
	public Integer getPelangganId() {
		return pelangganId;
	}
	public void setPelangganId(Integer pelangganId) {
		this.pelangganId = pelangganId;
	}
	@Column(name="TOTAL")
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	@Column(name="USER_ID")
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	@OneToMany(fetch=FetchType.EAGER, mappedBy="penjualan")
	public List<PenjualanDetailModel> getDetailPenjualans() {
		return detailPenjualans;
	}
	public void setDetailPenjualans(List<PenjualanDetailModel> detailPenjualans) {
		this.detailPenjualans = detailPenjualans;
	}
	
	
}
