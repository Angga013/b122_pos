package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="DESA")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="FAK_ID")
public class DesaModel{
	private Integer id;
	private String nama;
	private Integer kecamatanId;
	
	@Id
	@Column(name="DESA_ID")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="DESA")
	@TableGenerator(name="DESA",table="SEQUENCE", pkColumnName="SEQUENCE_ID", pkColumnValue="DESA", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="DESA_NAME")
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	@Column(name="KECAMATAN_ID")
	public Integer getKecamatanId() {
		return kecamatanId;
	}
	public void setKecamatanId(Integer kecamatanId) {
		this.kecamatanId = kecamatanId;
	}
	
	
}
