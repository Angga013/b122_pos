package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="KABUPATEN")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="FAK_ID")
public class KabupatenModel {
	private Integer id;
	private String nama;
	private Integer propinsiId;
	
	@Id
	@Column(name="KABUPATEN_ID")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="KABUPATEN")
	@TableGenerator(name="KABUPATEN",table="SEQUENCE", pkColumnName="SEQUENCE_ID", pkColumnValue="KABUPATEN", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="KABUPATEN_NAME")
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	@Column(name="PROPINSI_ID")
	public Integer getPropinsiId() {
		return propinsiId;
	}
	public void setPropinsiId(Integer propinsiId) {
		this.propinsiId = propinsiId;
	}
}
