package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="PENJUALAN_DETAIL")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
public class PenjualanDetailModel {
	private Integer id;
	private Integer penjualanId;
	private Integer cdId;
	private Integer jumlah;
	private Integer subTotal;
	private PenjualanModel penjualan;

	private CdModel cModel;
	
	
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="PENJUALAN_DETAIL")
	@TableGenerator(name="PENJUALAN_DETAIL",table="SEQUENCE", pkColumnName="SEQUENCE_ID", pkColumnValue="PENJUALAN_DETAIL", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="PENJUALAN_ID")
	public Integer getPenjualanId() {
		return penjualanId;
	}
	public void setPenjualanId(Integer penjualanId) {
		this.penjualanId = penjualanId;
	}
	@Column(name="CD_ID")
	public Integer getCdId() {
		return cdId;
	}
	public void setCdId(Integer cdId) {
		this.cdId = cdId;
	}
	@Column(name="JUMLAH")
	public Integer getJumlah() {
		return jumlah;
	}
	public void setJumlah(Integer jumlah) {
		this.jumlah = jumlah;
	}
	@Column(name="SUB_TOTAL")
	public Integer getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(Integer subTotal) {
		this.subTotal = subTotal;
	}
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="PENJUALAN_ID", nullable=false, insertable=false, updatable=false)
	public PenjualanModel getPenjualan() {
		return penjualan;
	}
	public void setPenjualan(PenjualanModel penjualan) {
		this.penjualan = penjualan;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="CD_ID", insertable=false, nullable=false, updatable=false)
	public CdModel getcModel() {
		return cModel;
	}
	public void setcModel(CdModel cModel) {
		this.cModel = cModel;
	}
	

}
