/**
 * 
 */
package com.xsis.bootcamp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author ASUS
 *
 */
@Entity
@Table(name="PEMBELIAN")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
public class PembelianModel {
	private Integer id;
	private String nomorFaktur;
	private Integer distributorId;
	private Date tanggalPembelian;
	private Integer total;
	private List<PembelianDetailModel> detailPembelians;
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="PEMBELIAN")
	@TableGenerator(name="PEMBELIAN",table="SEQUENCE",pkColumnName="SEQUENCE_ID",pkColumnValue="PEMBELIAN",valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="NOMOR_FAKTUR")
	public String getNomorFaktur() {
		return nomorFaktur;
	}
	public void setNomorFaktur(String nomorFaktur) {
		this.nomorFaktur = nomorFaktur;
	}
	
	@Column(name="DISTRIBUTOR_ID")
	public Integer getDistributorId() {
		return distributorId;
	}
	public void setDistributorId(Integer distributorId) {
		this.distributorId = distributorId;
	}
	
	@Column(name="TANGGAL_PEMBELIAN")
	@Temporal(TemporalType.DATE)
	public Date getTanggalPembelian() {
		return tanggalPembelian;
	}
	public void setTanggalPembelian(Date tanggalPembelian) {
		this.tanggalPembelian = tanggalPembelian;
	}
	
	@Column(name="TOTAL")
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}	
	
	@Transient
	public List<PembelianDetailModel> getDetailPembelians() {
		return detailPembelians;
	}
	public void setDetailPembelians(List<PembelianDetailModel> detailPembelians) {
		this.detailPembelians = detailPembelians;
	}
}
