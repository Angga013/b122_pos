/**
 * 
 */
package com.xsis.bootcamp.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author ASUS
 *
 */

@Entity
@Table(name="DISTRIBUTOR")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
public class DistributorModel {
	private Integer id;
	private String namaDistributor;
	private String alamatDistributor;
	private Integer kecamatanId;
	private Integer kotaId;
	private Integer provinsiId;
	private Integer noTelepon;
	private Integer noFax;
	private String email;
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="DISTRIBUTOR")
	@TableGenerator(name="DISTRIBUTOR",table="SEQUENCE", pkColumnName="SEQUENCE_ID", pkColumnValue="DISTRIBUTOR", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="NAMA_DISTRIBUTOR")
	public String getNamaDistributor() {
		return namaDistributor;
	}
	public void setNamaDistributor(String namaDistributor) {
		this.namaDistributor = namaDistributor;
	}
	
	@Column(name="ALAMAT_DISTRIBUTOR")
	public String getAlamatDistributor() {
		return alamatDistributor;
	}
	public void setAlamatDistributor(String alamatDistributor) {
		this.alamatDistributor = alamatDistributor;
	}
	
	@Column(name="KECAMATAN_ID")
	public Integer getKecamatanId() {
		return kecamatanId;
	}
	public void setKecamatanId(Integer kecamatanId) {
		this.kecamatanId = kecamatanId;
	}
	
	@Column(name="KOTA_ID")
	public Integer getKotaId() {
		return kotaId;
	}
	public void setKotaId(Integer kotaId) {
		this.kotaId = kotaId;
	}
	
	@Column(name="PROVINSI_ID")
	public Integer getProvinsiId() {
		return provinsiId;
	}
	public void setProvinsiId(Integer provinsiId) {
		this.provinsiId = provinsiId;
	}
	
	@Column(name="NO_TELEPON")
	public Integer getNoTelepon() {
		return noTelepon;
	}
	public void setNoTelepon(Integer noTelepon) {
		this.noTelepon = noTelepon;
	}
	
	@Column(name="NO_FAX")
	public Integer getNoFax() {
		return noFax;
	}
	public void setNoFax(Integer noFax) {
		this.noFax = noFax;
	}
	
	@Column(name="EMAIL")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
