package com.xsis.bootcamp.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="CD")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
public class CdModel {
	private Integer id;
	private String namaCd;
	private Integer harga;
	private Integer hargaBeli;
	private Integer kategoriId;
	private Integer genreId;
	private Integer distributorId;
	
	private StokCdModel stokModel;
	private Set<PenjualanDetailModel> pDetModel;
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="CD")
	@TableGenerator(name="CD",table="SEQUENCE", pkColumnName="SEQUENCE_ID", pkColumnValue="CD", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="HARGA_BELI")
	public Integer getHargaBeli() {
		return hargaBeli;
	}
	public void setHargaBeli(Integer hargaBeli) {
		this.hargaBeli = hargaBeli;
	}

	@Column(name="CD_NAME")
	public String getNamaCd() {
		return namaCd;
	}
	public void setNamaCd(String namaCd) {
		this.namaCd = namaCd;
	}
	
	
	@Column(name="HARGA")
	public Integer getHarga() {
		return harga;
	}
	public void setHarga(Integer harga) {
		this.harga = harga;
	}
	
	@Column(name="KATEGORI_ID")
	public Integer getKategoriId() {
		return kategoriId;
	}
	public void setKategoriId(Integer kategoriId) {
		this.kategoriId = kategoriId;
	}
	
	@Column(name="GENRE_ID")
	public Integer getGenreId() {
		return genreId;
	}
	public void setGenreId(Integer genreId) {
		this.genreId = genreId;
	}
	
	@Column(name="DISTRIBUTOR_ID")
	public Integer getDistributorId() {
		return distributorId;
	}
	public void setDistributorId(Integer distributorId) {
		this.distributorId = distributorId;
	}

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "cModel")
	public StokCdModel getStokModel() {
		return stokModel;
	}
	public void setStokModel(StokCdModel stokModel) {
		this.stokModel = stokModel;
	}

	@OneToMany(fetch=FetchType.EAGER, mappedBy="cModel")
	public Set<PenjualanDetailModel> getpDetModel() {
		return pDetModel;
	}
	public void setpDetModel(Set<PenjualanDetailModel> pDetModel) {
		this.pDetModel = pDetModel;
	}
}