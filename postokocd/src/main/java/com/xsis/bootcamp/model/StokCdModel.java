package com.xsis.bootcamp.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="STOK_CD")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
public class StokCdModel {

	private Integer id;
	private Integer cdId;
	private Integer jumlahStok;
	
	private CdModel cModel;
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="STOK_CD")
	@TableGenerator(name="STOK_CD",table="SEQUENCE", pkColumnName="SEQUENCE_ID", pkColumnValue="STOK_CD", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="CD_ID")
	public Integer getCdId() {
		return cdId;
	}
	public void setCdId(Integer cdId) {
		this.cdId = cdId;
	}
	
	@Column(name="JUMLAH_STOK")
	public Integer getJumlahStok() {
		return jumlahStok;
	}
	public void setJumlahStok(Integer jumlahStok) {
		this.jumlahStok = jumlahStok;
	}
	
	@OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.REMOVE, optional=true)
	@JoinColumn(name = "CD_ID", nullable = false, updatable = false, insertable = false)
	public CdModel getcModel() {
		return cModel;
	}
	public void setcModel(CdModel cModel) {
		this.cModel = cModel;
	}
	
	
	
}
