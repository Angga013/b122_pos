package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="GENRE")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
public class GenreModel {
	private Integer id;
	private String namaGenre;
	private Integer kategoriId;
	private String keterangan;
	private KategoriModel kategori;

	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="GENRE")
	@TableGenerator(name="GENRE",table="SEQUENCE", pkColumnName="SEQUENCE_ID", pkColumnValue="GENRE", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="NAMA_GENRE")
	public String getNamaGenre() {
		return namaGenre;
	}
	public void setNamaGenre(String namaGenre) {
		this.namaGenre = namaGenre;
	}
	
	@Column(name="KATEGORI_ID")
	public Integer getKategoriId() {
		return kategoriId;
	}
	public void setKategoriId(Integer kategoriId) {
		this.kategoriId = kategoriId;
	}
	
	@Column(name="KETERANGAN")
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="KATEGORI_ID", insertable=false, nullable=false, updatable=false)
	public KategoriModel getKategori() {
		return kategori;
	}
	public void setKategori(KategoriModel kategori) {
		this.kategori = kategori;
	}
	
}
