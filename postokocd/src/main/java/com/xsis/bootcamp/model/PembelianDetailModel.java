/**
 * 
 */
package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author ASUS
 *
 */

@Entity
@Table(name="PEMBELIAN_DETAIL")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
public class PembelianDetailModel {
	private Integer id;
	private Integer pembelianId;
	private Integer cdId;
	private Integer hargaSatuan;
	private Integer jumlahBeli;
	private Integer subTotal;
	
	
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="PEMBELIAN_DETAIL")
	@TableGenerator(name="PEMBELIAN_DETAIL", table="SEQUENCE", pkColumnName="SEQUENCE_ID", pkColumnValue="PEMBELIAN_DETAIL", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="PEMBELIAN_ID")
	public Integer getPembelianId() {
		return pembelianId;
	}
	public void setPembelianId(Integer pembelianId) {
		this.pembelianId = pembelianId;
	}
	
	@Column(name="CD_ID")
	public Integer getCdId() {
		return cdId;
	}
	public void setCdId(Integer cdId) {
		this.cdId = cdId;
	}
	
	@Column(name="HARGA_SATUAN")
	public Integer getHargaSatuan() {
		return hargaSatuan;
	}
	public void setHargaSatuan(Integer hargaSatuan) {
		this.hargaSatuan = hargaSatuan;
	}
	
	@Column(name="JUMLAH_BELI")
	public Integer getJumlahBeli() {
		return jumlahBeli;
	}
	public void setJumlahBeli(Integer jumlahBeli) {
		this.jumlahBeli = jumlahBeli;
	}
	
	@Column(name="SUBTOTAL")
	public Integer getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(Integer subTotal) {
		this.subTotal = subTotal;
	}
	
	
	
	
}
