package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.ProvinsiDao;
import com.xsis.bootcamp.model.ProvinsiModel;

@Repository
public class ProvinsiDaoImpl implements ProvinsiDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<ProvinsiModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<ProvinsiModel> result = session.createQuery("from ProvinsiModel").list();
		return result;
	}

	@Override
	public void insert(ProvinsiModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public ProvinsiModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		ProvinsiModel result = session.get(ProvinsiModel.class, id);
		
		return result;
	}

	@Override
	public void update(ProvinsiModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(ProvinsiModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

}
