package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.xsis.bootcamp.dao.CdDao;
import com.xsis.bootcamp.model.CdModel;
import com.xsis.bootcamp.model.KecamatanModel;

@Repository
public class CdDaoImpl implements CdDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<CdModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<CdModel> result = session.createQuery("from CdModel").list();
		return result;
	}

	@Override
	public void insert(CdModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public CdModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		CdModel result = session.get(CdModel.class, id);
		
		return result;
	}

	@Override
	public void update(CdModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(CdModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

	@Override
	public List<CdModel> getByKategoriId(int id) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(CdModel.class);
		criteria.add(Restrictions.eq("kategoriId",id));
		List<CdModel> result = criteria.list();
		return result;
	}

	@Override
	public List<CdModel> getByGenreId(int id) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(CdModel.class);
		criteria.add(Restrictions.eq("genreId",id));
		List<CdModel> result = criteria.list();
		return result;
	}

	@Override
	public List<CdModel> getByDistributorId(int id) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(CdModel.class);
		criteria.add(Restrictions.eq("distributorId",id));
		List<CdModel> result = criteria.list();
		return result;
	}

}
