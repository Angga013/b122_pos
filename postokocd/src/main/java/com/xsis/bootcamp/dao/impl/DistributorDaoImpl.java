/**
 * 
 */
package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.DistributorDao;
import com.xsis.bootcamp.model.DistributorModel;

/**
 * @author ASUS
 *
 */

@Repository
public class DistributorDaoImpl implements DistributorDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<DistributorModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<DistributorModel> result = session.createQuery("from DistributorModel").list();
		return result;
	}

	@Override
	public void insert(DistributorModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public DistributorModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		DistributorModel result = session.get(DistributorModel.class, id);
		return result;
	}

	@Override
	public void update(DistributorModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(DistributorModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

}
