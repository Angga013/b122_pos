package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.xsis.bootcamp.dao.PelangganDao;
import com.xsis.bootcamp.model.PelangganModel;

@Repository
public class PelangganDaoImpl implements PelangganDao {

	@Autowired
	SessionFactory sess;
	
	
	@Override
	public List<PelangganModel> get() throws Exception {
		Session session = sess.getCurrentSession();
		List<PelangganModel> result = session.createQuery("From PelangganModel").list();
		return result;
	}

	@Override
	public void insert(PelangganModel model) throws Exception {
		Session session = sess.getCurrentSession();
		session.save(model);
	}


	@Override
	public void update(PelangganModel model) throws Exception {
		Session session = sess.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(PelangganModel model) throws Exception {
		Session session = sess.getCurrentSession();
		session. delete(model);
	}

	@Override
	public PelangganModel getById(Integer id) throws Exception {
		Session session = sess.getCurrentSession();
		PelangganModel result = session.get(PelangganModel.class, id);
		return result;
	}

}
