package com.xsis.bootcamp.dao.impl;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.xsis.bootcamp.dao.PenjualanDetailDao;
import com.xsis.bootcamp.model.PenjualanDetailModel;

@Repository
public class PenjualanDetailDaoImpl implements PenjualanDetailDao {
	
	@Autowired
	private SessionFactory sess;

	@Override
	public List<PenjualanDetailModel> get() throws Exception {
		Session session = sess.getCurrentSession();
		List<PenjualanDetailModel> result = session.createQuery("From PenjualanDetailModel").list();
		return result;
	}

	@Override
	public void insert(PenjualanDetailModel model) throws Exception {
		Session session = sess.getCurrentSession();
		session.save(model);
	}

	@Override
	public List<PenjualanDetailModel> getByPenjualanId(Integer id) throws Exception {
		Session session = sess.getCurrentSession();
		Criteria crit = session.createCriteria(PenjualanDetailModel.class);
		crit.add(Restrictions.eq("penjualanDetailId",id));
		List<PenjualanDetailModel> result = crit.list();
		return result;
	}

	@Override
	public void update(PenjualanDetailModel model) throws Exception {
		Session session = sess.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(PenjualanDetailModel model) throws Exception {
		Session session = sess.getCurrentSession();
		session.delete(model);
	}
	
	
}
