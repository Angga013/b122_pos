package com.xsis.bootcamp.dao;

import java.util.List;
import com.xsis.bootcamp.model.PenjualanDetailModel;

public interface PenjualanDetailDao {
	public List<PenjualanDetailModel> get() throws Exception;
	public void insert (PenjualanDetailModel model) throws Exception;
	public List<PenjualanDetailModel> getByPenjualanId(Integer id) throws Exception;
	public void update(PenjualanDetailModel model) throws Exception;
	public void delete(PenjualanDetailModel model) throws Exception;
		
}
