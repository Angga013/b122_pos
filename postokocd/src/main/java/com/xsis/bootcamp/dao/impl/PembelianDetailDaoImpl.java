/**
 * 
 */
package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.PembelianDetailDao;
import com.xsis.bootcamp.model.PembelianDetailModel;

/**
 * @author ASUS
 *
 */

@Repository
public class PembelianDetailDaoImpl implements PembelianDetailDao {
	@Autowired
	private SessionFactory sessionFactory;	
	
	@Override
	public List<PembelianDetailModel> get() throws Exception {
		Session session =this.sessionFactory.getCurrentSession();
		List<PembelianDetailModel> result = session.createQuery("from PembelianDetailModel").list();
		return result;
	}

	@Override
	public List<PembelianDetailModel> getByPembelianId(Integer id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(PembelianDetailModel.class);
		criteria.add(Restrictions.eq("pembelianId", id));
		List<PembelianDetailModel> result = criteria.list();
		return result;
	}

	@Override
	public List<PembelianDetailModel> getByCdId(Integer id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(PembelianDetailModel.class);
		List<PembelianDetailModel> result = criteria.list();
		return result;
	}

	@Override
	public void insert(PembelianDetailModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public PembelianDetailModel getById(Integer id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		PembelianDetailModel result = session.get(PembelianDetailModel.class, id);
		return result;
	}

	@Override
	public void update(PembelianDetailModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(PembelianDetailModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}
}
