package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.xsis.bootcamp.dao.StokCdDao;
import com.xsis.bootcamp.model.StokCdModel;

@Repository
public class StokCdDaoImpl implements StokCdDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<StokCdModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<StokCdModel> result = session.createQuery("from StokCdModel").list();
		return result;
	}

	@Override
	public void insert(StokCdModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public StokCdModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		StokCdModel result = session.get(StokCdModel.class, id);
		
		return result;
	}

	@Override
	public void update(StokCdModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(StokCdModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

}
