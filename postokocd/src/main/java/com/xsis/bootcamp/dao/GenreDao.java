package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.GenreModel;

public interface GenreDao {
	public List<GenreModel> get() throws Exception;
	public void insert(GenreModel model) throws Exception;
	public GenreModel getById(int id) throws Exception;
	public void update(GenreModel model) throws Exception;
	public void delete(GenreModel model) throws Exception;
}
