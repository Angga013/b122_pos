package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.ProvinsiModel;

public interface ProvinsiDao {
	public List<ProvinsiModel> get() throws Exception;
	public void insert(ProvinsiModel model) throws Exception;
	public ProvinsiModel getById(int id) throws Exception;
	public void update(ProvinsiModel model) throws Exception;
	public void delete(ProvinsiModel model) throws Exception;
}
