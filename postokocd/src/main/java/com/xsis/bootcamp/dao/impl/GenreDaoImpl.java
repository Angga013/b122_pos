package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.xsis.bootcamp.dao.GenreDao;
import com.xsis.bootcamp.model.GenreModel;

@Repository
public class GenreDaoImpl implements GenreDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<GenreModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<GenreModel> result = session.createQuery("from GenreModel").list();
		return result;
	}

	@Override
	public void insert(GenreModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public GenreModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		GenreModel result = session.get(GenreModel.class, id);
		
		return result;
	}

	@Override
	public void update(GenreModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(GenreModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

}
