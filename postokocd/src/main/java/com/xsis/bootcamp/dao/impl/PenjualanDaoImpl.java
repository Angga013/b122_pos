package com.xsis.bootcamp.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.CdDao;
import com.xsis.bootcamp.dao.PenjualanDao;
import com.xsis.bootcamp.model.CdModel;
import com.xsis.bootcamp.model.PenjualanDetailModel;
import com.xsis.bootcamp.model.PenjualanModel;

@Repository
public class PenjualanDaoImpl implements PenjualanDao {

	@Autowired
	SessionFactory sess;
	
	@Override
	public List<PenjualanModel> get() throws Exception {
		Session session = sess.getCurrentSession();
		List<PenjualanModel> result = session.createQuery("From PenjualanModel").list();
		return result;
	}

	@Override
	public void insert(PenjualanModel model) throws Exception {
		Session session = sess.getCurrentSession();
		session.save(model);
	}

	@Override
	public PenjualanModel getById(Integer id) throws Exception {
		Session session  = sess.getCurrentSession();
		PenjualanModel result = session.get(PenjualanModel.class, id);
		return result;
	}

	@Override
	public List<PenjualanModel> getByTglTransaksi(Date tglTransaksi) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<PenjualanModel> getByUserId(Integer userId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(PenjualanModel model) throws Exception {
		Session session = sess.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(PenjualanModel model) throws Exception {
		Session session = sess.getCurrentSession();
		session.delete(model);
	}
}
