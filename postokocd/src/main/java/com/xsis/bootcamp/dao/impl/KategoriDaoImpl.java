package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.xsis.bootcamp.dao.KategoriDao;
import com.xsis.bootcamp.model.KategoriModel;

@Repository
public class KategoriDaoImpl implements KategoriDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<KategoriModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<KategoriModel> result = session.createQuery("from KategoriModel").list();
		return result;
	}

	@Override
	public void insert(KategoriModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public KategoriModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		KategoriModel result = session.get(KategoriModel.class, id);
		
		return result;
	}

	@Override
	public void update(KategoriModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(KategoriModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

}
