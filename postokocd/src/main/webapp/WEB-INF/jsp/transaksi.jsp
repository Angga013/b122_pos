<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">Tabel transaksi</h3>
		<div class="box-tools">
			<button type="button" id="btn-add" class="btn btn-primary pull-right">
				<i class="fa fa-plus"></i> Add transaksi
			</button>
		</div>
	</div>
	<div class="box-body">
		<table class="table">
			<thead>
				<tr>
					<th>Nomor Faktur</th>
					<th>Pelanggan</th>
					<th>Tanggal Transaksi</th>
					<th>Total</th>
					<th>Kasir</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
		</table>
	</div>
</div>

<!-- Modal -->
<div id="modal-input" class="modal">
	<div class="modal-dialog" style="width: 95%; min-height: 95%">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4 id="label_modal"></h4>
			</div>
			<div class="modal-body"></div>

		</div>
	</div>
</div>
<!-- Modal -->
<div id="modal-search-pelanggan" class="modal">
	<div class="modal-dialog" style="width: 95%;">
		<div class="modal-content" style="margin: auto;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Daftar Pelanggan</h4>

			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>
<!-- Modal -->
<div id="modal-search-cd" class="modal">
	<div class="modal-dialog" style="width: 95%;">
		<div class="modal-content" style="margin: auto;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Daftar CD/DVD</h4>
				<div class="modal-body"></div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="modal-detail-pembelian" class="modal">
	<div class="modal-dialog" style="width: 70%;">
		<div class="modal-content" style="margin: auto;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Rincian Pembelian</h4>
				<div class="modal-body"></div>
			</div>
		</div>
	</div>
</div>

<script>
	function loadData() {
		$.ajax({
			url : 'transaksi/list.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$("#list-data").html(data);
			}
		});
	}
	
	function loadPelanggan() {
		$.ajax({
			url : 'transaksi/listPelanggan.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$("#modal-search-pelanggan .modal-body").html(data);
				$("#modal-search-pelanggan").modal('show');
			}
		});
	}

	function loadBuku() {
		$.ajax({
			url : 'transaksi/cdSearch.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$("#modal-search-cd .modal-body").html(data);
				$("#modal-search-cd").modal('show');
			}
		});
	}

	loadData();
	//Tambah transaksi
	$("#btn-add").on("click", function() {
		$.ajax({
			url : 'transaksi/add.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$("#modal-input").find(".modal-body").html(data);
				$("#modal-input").modal('show');
			}
		});
		$("#label_modal").text("Tambah transaksi Baru");
	});

	//Cari Supplier
	$("#modal-input").on("click", "#btn-search", function() {
		loadPelanggan();
	});

	//Pilih Supplier
	$("#modal-search-pelanggan").on("click", ".btn-pilih", function() {
		var namaPelanggan = $(this).attr("data-nama");
		var alamat = $(this).attr("data-alamat");
		var noTelp = $(this).attr("data-telp");
		var email = $(this).attr("data-email");

		$("#modal-input #namaPelanggan").val(namaPelanggan);
		$("#modal-input #alamat").val(alamat);
		$("#modal-input #noTelp").val(noTelp);
		$("#modal-input #email").val(email);
		$("#modal-input #pelangganId").val($(this).val());

		$("#modal-search-pelanggan").modal('hide');
	});

	//Tambah Buku
	$("#modal-input").on("click", "#tambah-item", function() {
		loadBuku();
	});

	$("#modal-search-cd")
			.on(
					"click",
					".btn-pilih",
					function() {
						var index = $("#modal-input #listBeli >tr").length;
						var tr = '<tr>'
								+ '<td>'
								+ (index + 1)
								+ '</td>'
								+ '<td><input type="hidden" name="detailPenjualans['
								+ index
								+ '].cdId" value="'
								+ $(this).val()
								+ '">'
								+ $(this).attr("data-judulBuku")
								+ '</td>'
								+ '<td><input type="text" name="detailPenjualans['
								+ index
								+ '].harga" value="'
								+ $(this).attr("data-hargaJual")
								+ '" class="form-control hargaJual"></td>'
								+ '<td><input type="text" name="detailPenjualans['+index+'].jumlah" value="" class="form-control jumlahBeli"></td>'
								+ '<td><input type="text" name="detailPenjualans['+index+'].subTotal" value="" class="form-control subTotal"></td>'
								+ '<td><button type="button" class="btn btn-block btn-danger btn-xs hapus-item">Hapus</button></td>'
								+ '</tr>';

						$("#modal-input #listBeli").append(tr);
						$("#modal-search-cd").modal('hide');
					});

	$("#modal-input").on("keyup", ".jumlahBeli", function() {
		var jumlahBeli = $(this).val();
		var hargaJual = $(this).parent().parent().find(".hargaJual").val();
		var subTotal = jumlahBeli * hargaJual;

		$(this).parent().parent().find(".subTotal").val(subTotal);
	});
	
	
	$("#modal-input").on("keyup", ".jumlahBeli", function() {
		var grandTotal = 0;
		var jumlahBeli = $(this).val();
		var hargaJual = $(this).parent().parent().find(".hargaJual").val();
		var subTotal = jumlahBeli * hargaJual;
		grandTotal 	= grandTotal+subTotal;

		$(this).parent().parent().find(".subTotal").val(subTotal);
		$("#modal-input #grandTotal").val(grandTotal);
	});
	
	//Hapus Item Pembelian
	$("#modal-input").on("click", ".hapus-item", function(){
		var subTotal = $(this).parent().parent().find(".subTotal").val();
		grandTotal 	= grandTotal-subTotal;
		
		$("#modal-input #grandTotal").val(grandTotal);
		$(this).parent().parent().html("");
	});


	//Simpan transaksi
	$("#modal-input").on("submit", "#form-transaksi", function() {
		$.ajax({
			url : 'transaksi/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(data) {
				if (data.result == "berhasil") {
					$("#modal-input").modal('hide');
					loadData();
				} else {
					$("#modal-input").modal('show');
				}
			}
		});
		return false;
	});
	
	$("#list-data").on("click", ".btn-rincian", function(){
		var detailTransaksiId = $(this).val();
		
		$.ajax({
			url			: 'transaksi/detailTransaksi.html',
			type 		: 'get',
			data 		: {id:detailTransaksiId},
			dataType 	: 'html',
			success		: function(data){
				$("#modal-detail-pembelian").find(".modal-body").html(data);
				$("#modal-detail-pembelian").modal("show");
			}
		});
	});
</script>
