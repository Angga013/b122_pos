<form id="form-distributor" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="update">
		<div class="form-group">
			<label class="control-label col-md-3">ID Distributor</label>
			<div class="col-md-8">
				<input type="text" id="id" name="id" class="form-control" value="${item.id}">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Nama Distributor</label>
			<div class="col-md-8">
				<input type="text" id="namaDistributor" name="namaDistributor" class="form-control" value="${item.namaDistributor}">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Alamat Distributor</label>
			<div class="col-md-8">
				<input type="text" id="alamatDistributor" name="alamatDistributor" class="form-control" value="${item.alamatDistributor}">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Provinsi</label>
			<div class="col-md-8">
				<select id="provinsiId" name="provinsiId" class="form-control">
					<option  value="${item.provinsiId}">Pilih Provinsi</option>
										
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Kota</label>
			<div class="col-md-8">
				<select id="kotaId" name="kotaId" class="form-control">
					<option value="${item.kotaId}">Pilih Kota</option>
					
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Kecamatan</label>
			<div class="col-md-8">
				<select id="kecamatanId" name="kecamatanId" class="form-control">
					<option value="${item.kecamatanId}">Pilih Kecamatan</option>
					
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">No. Telepon</label>
			<div class="col-md-8">
				<input type="text" id="noTelepon" name="noTelepon" class="form-control" value="${item.noTelepon}">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">No. Fax</label>
			<div class="col-md-8">
				<input type="text" id="noFax" name="noFax" class="form-control" value="${item.noFax}">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Email</label>
			<div class="col-md-8">
				<input type="email" id="email" name="email" class="form-control" value="${item.email}">
			</div>
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-warning">Perbarui</button>
	</div>
</form>
<script type="text/javascript">
function loadProvinsi(selectedId) {
	$.ajax({
		url : 'ajax/getProvinsi.json',
		type : 'get',
		dataType : 'json',
		success : function(data) {
			$("#modal-input #provinsiId").empty();
			$("#modal-input #provinsiId").append('<option value="">Pilih Provinsi</option>');
			$.each(data.result, function(index, item) {
				if (selectedId == item.id) {
					$("#modal-input #provinsiId").append('<option value="'+ item.id +'" selected="selected">'+ item.namaProvinsi + '</option>');
				} else {
					$("#modal-input #provinsiId").append('<option value="'+ item.id +'">'+ item.namaProvinsi + '</option>');
				}
				
			});
		}
	});
}

function loadKota(id, selectedId) {
	$.ajax({
		url : 'ajax/getKota.json',
		type : 'get',
		data : {
			id : id
		},
		dataType : 'json',
		success : function(data) {
			$("#modal-input #kotaId").empty();
			$("#modal-input #kotaId").append(
					'<option value="">Pilih Kota</option>');
			$.each(data.result, function(index, item) {
				if (selectedId == item.id) {
					$("#modal-input #kotaId").append('<option value="'+ item.id +'" selected="selected">' + item.namaKota + '</option>');
				} else {
					$("#modal-input #kotaId").append('<option value="'+ item.id +'">' + item.namaKota + '</option>');	
				}				
			});
		}
	});
}

function loadKecamatan(id, selectedId) {
	$.ajax({
		url : 'ajax/getKecamatan.json',
		type : 'get',
		data : {
			id : id
		},
		dataType : 'json',
		success : function(data) {
			$("#modal-input #kecamatanId").empty();
			$("#modal-input #kecamatanId").append(
					'<option value="">Pilih Kecamatan</option>');
			$.each(data.result, function(index, item) {
				if ( selectedId == item.id) {
					$("#modal-input #kecamatanId").append('<option value="'+ item.id +'" selected="selected">'+ item.namaKecamatan + '</option>');
				} else {
					$("#modal-input #kecamatanId").append('<option value="'+ item.id +'">'+ item.namaKecamatan + '</option>');
				}				
			});
		}
	});
}

$(document).ready(function(){
	loadProvinsi("${item.provinsiId}");
	loadKota("${item.provinsiId}","${item.kotaId}");
	loadKecamatan("${item.kotaId}","${item.kecamatanId}")
});

</script>
