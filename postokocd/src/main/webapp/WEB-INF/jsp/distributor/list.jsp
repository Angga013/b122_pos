<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach var="item" items="${list}">
	<tr>
		<td>${item.id}</td>
		<td>${item.namaDistributor}</td>
		<td>${item.alamatDistributor}</td>
		<td>${item.kecamatanId}</td>
		<td>${item.kotaId}</td>
		<td>${item.provinsiId}</td>
		<td>${item.noTelepon}</td>
		<td>${item.noFax}</td>
		<td>${item.email}</td>
		<td>
			<button type="button" class="btn btn-success btn-xs btn-edit" value="${item.id }"><i class="fa fa-edit"></i></button>
			<button type="button" class="btn btn-danger btn-xs btn-delete" value="${item.id }"><i class="fa fa-trash"></i></button>
		</td>
	</tr>
</c:forEach>