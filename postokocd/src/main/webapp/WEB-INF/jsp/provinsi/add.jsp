<form id="form-provinsi" action="save" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="insert">
		<input type="hidden" id="id" name="id" class="form-control">		
		<div class="form-group">
			<label class="control-label col-md-2">Nama Provinsi</label>
			<div class="col-md-6">
				<input type="text" id="namaProvinsi" name="namaProvinsi" class="form-control">
			</div>					
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>