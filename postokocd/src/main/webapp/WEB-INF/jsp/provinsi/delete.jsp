<form id="form-provinsi" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control"	value="delete">
		<input type="hidden" id="id" name="id" class="form-control" value="${item.id }">
		<input type="hidden" id="namaProvinsi" name="namaProvinsi" class="form-control" value="${item.namaProvinsi}">

		<div class="form-group">
			<div class="col-md-12">
				Apakah anda yakin mau menghapus data provinsi <b>${item.namaProvinsi}</b> dengan ID: <b>${item.id}</b>?
			</div>
		</div>

		<div class="modal-footer">
			<button type="submit" class="btn btn-danger">Hapus</button>
		</div>
	</div>
</form>
