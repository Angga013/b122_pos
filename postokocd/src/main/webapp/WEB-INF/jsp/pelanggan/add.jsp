<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-pelanggan" action="save" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control"
			value="insert">

		<div class="form-group">
			<label class="control-label col-md-3">Nama Pelanggan</label>
			<div class="col-md-8">
				<input type="text" id="namaPelanggan" name="namaPelanggan"
					class="form-control">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Alamat Pelanggan</label>
			<div class="col-md-8">
				<input type="text" id="alamatPelanggan" name="alamat"
					class="form-control">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Provinsi</label>
			<div class="col-md-8">
				<select id="provinsiId" name="provinsiId" class="form-control">
					<option>Pilih Provinsi</option>
					
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Kota</label>
			<div class="col-md-8">
				<select id="kotaId" name="kotaId" class="form-control">
					<option>Pilih Kota</option>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Kecamatan</label>
			<div class="col-md-8">
				<select id="kecamatanId" name="kecamatanId" class="form-control">
					<option>Pilih Kecamatan</option>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">No. Telepon</label>
			<div class="col-md-8">
				<input type="text" id="noTelp" name="noTelp"
					class="form-control">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3">Email</label>
			<div class="col-md-8">
				<input type="email" id="email" name="email" class="form-control">
			</div>
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>