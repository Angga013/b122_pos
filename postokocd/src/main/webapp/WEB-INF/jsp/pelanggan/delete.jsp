<form id="form-pelanggan" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control"	value="delete">
		<input type="hidden" id="id" name="id" class="form-control" value="${item.id }">

		<div class="form-group">
			<div class="col-md-12">
				Apakah anda yakin mau menghapus data Pelanggan <b>${item.namaPelanggan}</b>
			</div>
		</div>

		<div class="modal-footer">
			<button type="submit" class="btn btn-danger">Hapus</button>
		</div>
	</div>
</form>
