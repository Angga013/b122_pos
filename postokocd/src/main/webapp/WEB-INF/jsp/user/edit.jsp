<form id="form-user" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="update">
		<div class="form-group">
			<label class="control-label col-md-2">ID</label>
			<div class="col-md-6">
				<input type="text" id="id" name="id" class="form-control" value="${item.id }">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Nama Lengkap</label>
			<div class="col-md-6">
				<input type="text" id="namaLengkap" name="namaLengkap" class="form-control" value="${item.namaLengkap }">
			</div>					
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Username</label>
			<div class="col-md-6">
				<input type="text" id="username" name="username" class="form-control" value="${item.username }">
			</div>					
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Password</label>
			<div class="col-md-6">
				<input type="text" id="password" name="password" class="form-control" value="${item.password }">
			</div>					
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Role ID</label>
			<div class="col-md-6">
				<input type="text" id="roleId" name="roleId" class="form-control" value="${item.roleId }">
			</div>					
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-warning">Perbarui</button>
	</div>
</form>
