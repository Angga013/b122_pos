<form id="form-user" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete">
		<input type="hidden" id="id" name="id" class="form-control" value="${item.id }">
		
		<div class="form-group">
		<div>
		<p class="text-center">Apakah anda yakin mau menghapus user <b>${item.namaLengkap}</b> ?</p>	
		</div>
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-danger">Hapus</button>
	</div>
</form>
