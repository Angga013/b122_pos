<form id="form-buku" action="update" method="post">
	<div class="form-horizontal">
	<!-- Tarik data dengan variabel item yang dikirim controller, simpan pada form hidden -->
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete">
		<input type="hidden" id="id" name="id" value="${item.id}">
		<div class="form-group">
			<div class="col-md-12">
				<p>Buku dengan judul <b>${item.judulBuku}</b> akan dihapus, lanjutakan?</p>
			</div>

		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-danger">Lanjutkan Hapus</button>
	</div>
</form>
