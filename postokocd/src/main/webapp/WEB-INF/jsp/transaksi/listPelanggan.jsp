<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<table class="table">
	<thead>
		<tr>
			<th>Nama Pelanggan</th>
			<th>Alamat</th>
			<th>No Telp</th>
			<th>Email</th>
		</tr>
	</thead>
	<tbody id="list-data-pelanggan">
		<c:forEach var="item" items="${list}">
			<tr>
				<td>${item.namaPelanggan}</td>
				<td>${item.alamat}</td>
				<td>${item.noTelp}</td>
				<td>${item.email}</td>
				<td>
					<button type="button"
						class="btn btn-success btn-xs btn-edit btn-pilih"
						value="${item.id }" 
						data-nama="${item.namaPelanggan}"
						data-alamat="${item.alamat}"
						data-telp="${item.noTelp}"
						data-email="${item.email}">
						<i class="fa fa-check"></i>
					</button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>