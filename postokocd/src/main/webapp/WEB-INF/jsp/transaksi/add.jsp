<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-transaksi" class="form-horizontal" action="save"
	method="post">
	<input type="hidden" id="proses" name="proses" class="form-control"
		value="insert">
	<input type="hidden" id="kasir" name="kasir" class="form-control"
		value="2">
	<div class="row">
		<div class="col-md-6">
			<input type="hidden" id="pelangganId" name="pelangganId">
			<div class="form-group">
				<label class="control-label col-md-3">Nama Pelanggan</label>
				<div class="col-md-8">
					<div class="input-group input-group-sm">
						<input type="text" name="namaPelanggan" id="namaPelanggan"
							class="form-control" readonly="readonly" /> <span
							class="input-group-btn">
							<button type="button" id="btn-search"
								class="btn btn-info btn-flat">Go!</button>
						</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Alamat</label>
				<div class="col-md-8">
					<textarea class="form-control" rows="3" readonly="readonly"
						id="alamat"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Nomor Faktur</label>
				<div class="col-md-8">
					<input type="text" name="noFaktur" id="noFaktur"
						class="form-control" />
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-md-3">No. Telepon</label>
				<div class="col-md-8">
					<input type="text" class="form-control" readonly="readonly"
						id="noTelp" name="noTelp">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Email</label>
				<div class="col-md-8">
					<input type="text" name="email" id="email"
						class="form-control" readonly="readonly">
				</div>
			</div>
			<div class="modal-footer">
				<br><br>
				<button type="button" class="btn btn-success" id="tambah-item">Tambah CD/DVD</button>
			</div>
		</div>
	</div>

	<div class="box box-success">
		<div class="box-header">
			<h3 class="box-title">List item yang dibeli</h3>
		</div>
		<div class="form-group">
		<label class="control-label col-md-1" id=grandTotal>Grand Total</label>
		<div class="col-md-4">
			<input type="text" class="form-control" readonly="readonly"
				id="grandTotal" name="grandTotal">
		</div>
	</div>
		<div class="box-body no-padding">
			<table class="table table-condensed">
				<thead>
					<tr>
						<th style="width: 10px">#</th>
						<th>Judul CD/DVD</th>
						<th>Harga</th>
						<th>Jumlah</th>
						<th>Sub Total</th>
					</tr>
				</thead>
				<tbody id="listBeli">
				</tbody>
			</table>
		</div>
	</div>
	
	<div class="modal-footer">
		<button type="submit" class="btn btn-primary">Simpan
			Pembelian</button>
	</div>
	
</form>