<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<table class="table">
	<thead>
		<tr>
			<th>Id</th>
			<th>Judul CD/DVD</th>
			<th>Kategori</th>
			<th>Genre</th>
			<th>Harga</th>
		</tr>
	</thead>
	<tbody id="list-data-cd">
		<c:forEach var="item" items="${list}">
			<tr>
				<td>${item.id}</td>
				<td>${item.namaCd}</td>
				<td>${item.kategoriId}</td>
				<td>${item.genreId}</td>
				<td>${item.harga}</td>
				<td>
					<button type="button"
						class="btn btn-success btn-xs btn-pilih"
						value="${item.id }" data-judulBuku="${item.namaCd}"
						data-hargaJual="${item.harga}">
						<i class="fa fa-edit"></i>
					</button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>