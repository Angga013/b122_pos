<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach var="item" items="${list}">
	<tr>
		<td>${item.noFaktur}</td>
		<td>${item.pelangganId}</td>
		<td>${item.tglTransaksi}</td>
		<td>${item.total}</td>
		<td>${item.userId}</td>
		<td>
			<button type="button" class="btn btn-success btn-xs btn-rincian"
				value="${item.id }">
				<i class="fa fa-edit"></i>
				rincian
			</button>
		</td>
	</tr>
</c:forEach>