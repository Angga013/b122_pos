<form id="form-pembelian" class="form-horizontal" action="save"
	method="post">
	<input type="hidden" id="proses" name="proses" class="form-control"
		value="insert">
	<div class="row">
		<div class="col-md-6">
			<input type="hidden" id="distributorId" name="distributorId">
			<div class="form-group">
				<label class="control-label col-md-3">Nama Distributor</label>
				<div class="col-md-8">
					<div class="input-group input-group-sm">
						<input type="text" name="namaDistributor" id="namaDistributor"
							required="required" class="form-control" readonly="readonly" /> <span
							class="input-group-btn">
							<button type="button" id="btn-search"
								class="btn btn-info btn-flat">Go!</button>
						</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Alamat</label>
				<div class="col-md-8">
					<textarea class="form-control" rows="3" required="required" readonly="readonly"
						id="alamatDistributor"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Nomor Faktur</label>
				<div class="col-md-8">
					<input type="text" name="nomorFaktur" id="nomorFaktur" required="required"
						class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Tanggal Pembelian</label>
				<div class="col-md-8">
					<input type="date" name="tanggalPembelianStr"
						id="tanggalPembelianStr" required="required" class="form-control" />
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-md-3">No. Telepon</label>
				<div class="col-md-8">
					<input type="text" class="form-control" readonly="readonly"
						id="noTelepon" name="noTelepon">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">No. Fax</label>
				<div class="col-md-8">
					<input type="text" name="noFax" id="noFax" class="form-control"
						readonly="readonly">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Email</label>
				<div class="col-md-8">
					<input type="text" name="email" id="email" class="form-control"
						readonly="readonly">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="tambah-item">Tambah
					Pembelian CD</button>
			</div>
		</div>
	</div>

	<div class="box box-success">
		<div class="box-header">
			<h3 class="box-title">Data CD Yang Akan Dibeli</h3>
		</div>
		<div class="form-group">
			<label class="control-label col-md-1" id=grandTotal>Grand
				Total</label>
			<div class="col-md-4">
				<input type="text" class="form-control" readonly="readonly"
					id="grandTotal" name="grandTotal">
			</div>
		</div>
		<div class="box-body no-padding">
			<table class="table table-condensed">
				<thead>
					<tr>
						<th style="width: 10px">No.</th>
						<th>Nama CD</th>
						<th>Harga Satuan</th>
						<th>Jumlah Beli</th>
						<th>Sub Total</th>
					</tr>
				</thead>
				<tbody id="listBeli">
				</tbody>
			</table>
		</div>
	</div>
	<div class="modal-footer">
		<button type="submit" class="btn btn-primary">Simpan
			Pembelian</button>
	</div>
</form>