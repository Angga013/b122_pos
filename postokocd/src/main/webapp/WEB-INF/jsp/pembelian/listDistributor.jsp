<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<table class="table">
	<thead>
		<tr>
			<th>Nama Distributor</th>
			<th>Alamat</th>
			<th>No. Telepon</th>
			<th>No. Fax</th>
			<th>Email</th>
		</tr>
	</thead>
	<tbody id="list-data-distributor">
		<c:forEach var="item" items="${list}">
			<tr>
				<td>${item.namaDistributor}</td>
				<td>${item.alamatDistributor}
				<td>${item.noTelepon}</td>
				<td>${item.noFax}</td>
				<td>${item.email}</td>
				<td>
					<button type="button" class="btn btn-success btn-xs btn-pilih" value="${item.id }" data-nama="${item.namaDistributor}" data-alamat="${item.alamatDistributor}" data-telp="${item.noTelepon}" data-fax="${item.noFax}" data-email="${item.email}"><i class="fa fa-edit"></i></button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>