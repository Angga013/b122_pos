<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach var="item" items="${list}">
	<tr>
		<td>${item.id}</td>
		<td>${item.nomorFaktur}</td>
		<td>${item.distributorId}</td>
		<%-- <td>${item.alamatDistributor}</td> --%>
		<td>${item.tanggalPembelian}</td>
		<td>${item.total}</td>
		<td>
			<button type="button" class="btn btn-success btn-xs btn-rincian" value="${item.id }"><i class="fa fa-edit"></i>Rincian</button>
		</td>
	</tr>
</c:forEach>