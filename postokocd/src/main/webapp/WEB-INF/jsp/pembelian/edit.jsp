<form id="form-pembelian" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="update">
		<div class="form-group">
			<label class="control-label col-md-2">Detail ID</label>
			<div class="col-md-6">
				<input type="text" id="id" name="id" class="form-control" value="${item.id }">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Pembelian Id</label>
			<div class="col-md-6">
				<select id="pembelianId" name="pembelianId" class="form-control" value="${item.pembelianId}">
				<option>Pilih Pembelian ID</option>
				
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-2">Nama CD</label>
			<div class="col-md-6">
				<select id="cdId" name="cdId" class="form-control" value="${item.cdId}">
					<option>Pilih CD</option>

				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Harga Satuan</label>
			<div class="col-md-6">
				<input type="text" id="hargaSatuan" name="hargaSatuan" class="form-control" value="${item.hargaSatuan}">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-2">Jumlah Beli</label>
			<div class="col-md-6">
				<input type="text" id="jumlahBeli" name="jumlahBeli" class="form-control" value="${item.jumlahBeli}">
			</div>
		</div>
	</div>
	
	<div class="form-group">
			<label class="control-label col-md-2">Sub Total</label>
			<div class="col-md-6">
				<input type="text" id="subTotal" name="subTotal" class="form-control" value="${item.subTotal}">
			</div>
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-warning">Perbarui</button>
	</div>
</form>
<script>

function loadCd(selectedId){
	$.ajax({
		url			: 'ajax/getCd.json',
		type		: 'get',
		dataType	: 'json',
		success		: function(data){
			$("#modal-input #cdId").empty();
			$("#modal-input #cdId").append('<option value="">Pilih CD</option>');
			$.each(data.result,function(index,item){
				if (selectedId == item.id) {
					$("#modal-input #cdId").append('<option value="'+ item.id +'"selected=selected">'+ item.namaCd +'</option>');
				}
				$("#modal-input #cdId").append('<option value="'+ item.id +'">'+ item.namaCd +'</option>');
			});
		}
	});
}

$(document).ready(function(){
	loadCd("${item.cdId}");
});
</script>
