<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<table class="table" border="1">
	<thead>
		<tr>
			<th>Nama CD</th>
			<th align="right">Harga Satuan</th>
			<th align="right">Jumlah CD</th>
			<th align="right">Total Harga</th>
		</tr>
	</thead>
	<tbody id="list-data-pembelian">
		<c:forEach var="item" items="${list}">
			<tr>
				<td>${item.CdModel.namaCd}</td>
				<td align="right">${item.hargaSatuan}</td>
				<td align="right">${item.jumlahBeli}</td>
				<td align="right">${item.subTotal}</td>
			</tr>
		</c:forEach>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4" align="right">
				<h3>Total Pembelian: ${item.total}</h3>
			</td>
		</tr>
	</tfoot>
	
</table>