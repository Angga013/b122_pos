<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<table class="table">
	<thead>
		<tr>
			<th>ID</th>
			<th>Nama CD</th>
			<th>Nama Kategori</th>
			<th>Nama Genre</th>			
			<th>Nama Distributor</th>
			<th>Harga Beli</th>
		</tr>
	</thead>
	<tbody id="list-data-cd">
		<c:forEach var="item" items="${list}">
			<tr>
				<td>${item.id}</td>
				<td>${item.namaCd}</td>
				<td>${item.kategoriId}</td>
				<td>${item.genreId}</td>				
				<td>${item.distributorId}</td>
				<td>${item.harga}</td>
				<td>
					<button type="button" class="btn btn-success btn-xs btn-pilih" value="${item.id }" data-namaCd="${item.namaCd}" data-hargaBeli="${item.harga}"><i class="fa fa-edit"></i>
					</button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>