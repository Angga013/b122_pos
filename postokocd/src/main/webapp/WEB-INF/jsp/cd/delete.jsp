<form id="form-cd" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete">
		<input type="text" id="id" name="id" class="form-control" value="${item.id }">
		
		<div class="form-group">
			<p>Apakah anda yakin mau menghapus data CD/DVD ${item.namaCd} ?</p>				
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>
