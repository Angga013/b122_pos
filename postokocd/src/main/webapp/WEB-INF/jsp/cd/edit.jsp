<form id="form-cd" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control"
			value="update">
		<div class="form-group">
			<label class="control-label col-md-2">ID</label>
			<div class="col-md-6">
				<input type="text" id="id" name="id" required="required" class="form-control"
					value="${item.id }">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-2">Judul CD/DVD</label>
			<div class="col-md-6">
				<input type="text" id="namaCd" name="namaCd" required="required" class="form-control"
					value="${item.namaCd }">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Harga Beli</label>
			<div class="col-md-6">
				<input type="text" id="hargaBeli" name="hargaBeli" required="required" class="form-control"
					value="${item.hargaBeli }">
			</div>
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>
