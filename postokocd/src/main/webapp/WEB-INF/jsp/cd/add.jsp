<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-cd" action="save" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="insert">

		<div class="form-group">
			<label class="control-label col-md-3">Judul CD</label>
			<div class="col-md-8">
				<input type="text" id="namaCd" name="namaCd" required="required" class="form-control">
			</div>					
		</div>		
		<div class="form-group">
			<label class="control-label col-md-3">Harga Beli</label>
			<div class="col-md-8">
				<input type="text" id="hargaBeli" name="hargaBeli" required="required" class="form-control">
			</div>					
		</div>	
		<div class="form-group">
			<label class="control-label col-md-3">Kategori</label>
			<div class="col-md-8">
				<select id="kategoriId" name="kategoriId" required="required" class="form-control">
					<option>Pilih Kategori</option>							
					<c:forEach var="item" items="${listkat}">
						<option value="${item.id}"> ${item.namaKategori } </option>
					</c:forEach>
				</select>
			</div>					
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Genre</label>
			<div class="col-md-8">
				<select id="genreId" name="genreId" required="required" class="form-control">
					<option>Pilih Genre</option>							
					<c:forEach var="item" items="${listgen}">
						<option value="${item.id}"> ${item.namaGenre} </option>
					</c:forEach>
				</select>
			</div>					
		</div>
	<div class="form-group">
			<label class="control-label col-md-3">Distributor</label>
			<div class="col-md-8">
				<select id="distributorId" name="distributorId" required="required" class="form-control">
					<option>Pilih Distributor</option>							
					<c:forEach var="item" items="${listDistributor}">
						<option value="${item.id}"> ${item.namaDistributor } </option>
					</c:forEach>
				</select>
			</div>					
		</div>
		
		
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>