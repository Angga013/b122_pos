<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">Tabel Pembelian</h3>
		<div class="box-tools">
			<button type="button" id="btn-add" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add Pembelian</button>
		</div>
	</div>
	<div class="box-body">
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>No. Faktur</th>
					<th>Nama Distributor</th>
					<!-- <th>Alamat</th> -->
					<th>Tanggal Pembelian</th>
					<th>Total</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data">
			
			</tbody>
		</table>
	</div>
</div>

<!-- Modal -->
<div id="modal-input" class="modal">
	<div class="modal-dialog" style="width: 95%; min-height: 95%">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4 id="label_modal"></h4>
			</div>
			<div class="modal-body">
			
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="modal-search-distributor" class="modal">
	<div class="modal-dialog" style="width: 95%;">
		<div class="modal-content" style="margin: auto;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Data Distributor</h4>
			</div>
			<div class="modal-body">
			
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="modal-search-cd" class="modal">
	<div class="modal-dialog" style="width: 95%;">
		<div class="modal-content" style="margin: auto;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Data CD</h4>
			</div>
			<div class="modal-body">
			
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="modal-detail-pembelian" class="modal">
	<div class="modal-dialog" style="width: 70%;">
		<div class="modal-content" style="margin: auto;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Rincian Pembelian</h4>
				<div class="modal-body"></div>
			</div>
		</div>
	</div>
</div>

<script>
	function loadData(){
		$.ajax({
			url:'pembelian/list.html',
			type:'get',
			dataType:'html',
			success:function(data){
				$("#list-data").html(data);
			}
		});
	}
	
	function loadDistributor(){
		$.ajax({
			url:'pembelian/listDistributor.html',
			type:'get',
			dataType:'html',
			success:function(data){
				$("#modal-search-distributor .modal-body").html(data);
				$("#modal-search-distributor").modal('show');
			}
		});
	}
	
	function loadCd(){
		$.ajax({
			url:'pembelian/listCd.html',
			type:'get',
			dataType:'html',
			success:function(data){
				$("#modal-search-cd .modal-body").html(data);
				$("#modal-search-cd").modal('show');
			}
		});
	}
	
	loadData();
	
	// button add
	$(document).ready(function(){
		
		$("#btn-add").on("click",function(){
			$.ajax({
				url:'pembelian/add.html',
				type:'get',
				dataType:'html',
				success:function(data){
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
			$("#label_modal").text("Tambah Pembelian Baru");
		});
		
		// Cari distributor
		$("#modal-input").on("click", "#btn-search", function(){
			loadDistributor();
		});
		
		// Pilih distributor
		$("#modal-search-distributor").on("click", ".btn-pilih", function(){
			var namaDistributor = $(this).attr("data-nama");
			var alamatDistributor = $(this).attr("data-alamat");
			var noTelepon = $(this).attr("data-telp");
			var noFax = $(this).attr("data-fax");
			var email = $(this).attr("data-email");
			
			$("#modal-input #distributorId").val($(this).val());
			$("#modal-input #namaDistributor").val(namaDistributor);
			$("#modal-input #alamatDistributor").val(alamatDistributor);
			$("#modal-input #noTelepon").val(noTelepon);
			$("#modal-input #noFax").val(noFax);
			$("#modal-input #email").val(email);
			
			$("#modal-search-distributor").modal('hide');
		});
		
		// Cari CD
		$("#modal-input").on("click", "#tambah-item", function(){
			loadCd();			
		});
		
		// Tambah CD
		$("#modal-search-cd").on("click", ".btn-pilih", function(){
			var index = $("#modal-input #listBeli >tr").length;
			var tr = '<tr>'+
						'<td>' + (index + 1) + '</td>' +
						'<td><input type="hidden" name="detailPembelians['+ index +'].cdId" value="'+ $(this).val() +'">' + $(this).attr("data-namaCd") + '</td>' +
						'<td><input type="text" name="detailPembelians['+ index +'].hargaBeli" value="'+ $(this).attr("data-hargaBeli") +'" class="form-control hargaBeli"></td>' +
						'<td><input type="text" name="detailPembelians['+ index +'].jumlahBeli" value="" class="form-control jumlahBeli"></td>' +
						'<td><input type="text" name="detailPembelians['+ index +'].subTotal" value="" class="form-control subTotal"></td>' +
						'<td><button type="button" class="btn btn-block btn-danger btn-xs btn-delete">Hapus</button></td>' +
					'</tr>';
					
			$("#modal-input #listBeli").append(tr);
			$("#modal-search-cd").modal('hide');
			
			/* var namaCd = $(this).attr("data-namaCd");
			var cek = $("#modal-input #listBeli").val(namaCd);
			var dataCd = $("#modal-body #list-data-cd").model('show');
			
			
			if ( tr == dataCd) {
				$("#modal-body #").
			}
			 */ 
		});
		
		$("#modal-input").on("keypress", ".jumlahBeli", function(){
			var jumlahBeli = $(this).val();
			var hargaSatuan = $(this).parent().parent().find(".hargaBeli").val();
			var subTotal = jumlahBeli * hargaSatuan;
			var grandTotal = 0;
			grandTotal 	= grandTotal+subTotal;
			
			$(this).parent().parent().find(".subTotal").val(subTotal);
			$("#modal-input #grandTotal").val(grandTotal);
		});
		
		$("#modal-input").on("submit","#form-pembelian",function(){
			$.ajax({
				url:'pembelian/save.json',
				type:'post',
				data:$(this).serialize(),
				dataType:'json',
				success:function(data){
					if(data.result=="berhasil"){
						$("#modal-input").modal('hide');
						loadData();
					}
					else{
						$("#modal-input").modal('show');
					}
				}
			});
			return false;
		});
		
		$("#modal-input").on("click", ".btn-delete", function(){
			var subTotal = $(this).parent().parent().find(".subTotal").val();
			grandTotal 	= grandTotal-subTotal;
			
			$("#modal-input #grandTotal").val(grandTotal);
			$(this).parent().parent().html("");
		});
		
		$("#list-data").on("click", ".btn-rincian", function(){
			var detailPembelianId = $(this).val();
			
			$.ajax({
				url			: 'pembelian/detailPembelian.html',
				type 		: 'get',
				data 		: {id:detailPembelianId},
				dataType 	: 'html',
				success		: function(data){
					$("#modal-detail-pembelian").find(".modal-body").html(data);
					$("#modal-detail-pembelian").modal('show');
				}
			});
		});
	}); 
</script>