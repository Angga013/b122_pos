<form id="form-kota" action="save" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="insert">
		<input type="hidden" id="id" name="id" class="form-control">		
		<div class="form-group">
			<label class="control-label col-md-2">Nama Kota</label>
			<div class="col-md-6">
				<input type="text" id="namaKota" name="namaKota" class="form-control">
			</div>					
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">ID Provinsi</label>
			<div class="col-md-6">
				<input type="text" id="provinsiId" name="provinsiId" class="form-control">
			</div>					
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>