<form id="form-kota" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control"
			value="delete"> <input type="hidden" id="id" name="id"
			class="form-control" value="${item.id }"> <input
			type="hidden" id="namaKota" name="namaKota" class="form-control"
			value="${item.namaKota }">

		<div class="form-group">
			<div class="col-md-12">
				Apakah anda yakin mau menghapus data <b>${item.namaKota}</b> dengan
				ID: <b>${item.id}</b>?
			</div>
		</div>

		<div class="modal-footer">
			<button type="submit" class="btn btn-danger">Hapus</button>
		</div>
	</div>
</form>
