<form id="form-kota" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="update">
		<div class="form-group">
			<label class="control-label col-md-2">ID</label>
			<div class="col-md-6">
				<input type="text" id="id" name="id" class="form-control" value="${item.id }">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Nama Kota</label>
			<div class="col-md-6">
				<input type="text" id="namaKota" name="namaKota" class="form-control" value="${item.namaKota}">
			</div>					
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-warning">Perbarui</button>
	</div>
</form>
