<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-genre" action="save" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control"
			value="insert">
		<div class="form-group">
			<label class="control-label col-md-2">ID</label>
			<div class="col-md-6">
				<input type="text" id="id" name="id" class="form-control">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Kategori</label>
			<div class="col-md-6">
				<select id="kategoriId" name="kategoriId" class="form-control" required="required">
					<option value="">Silakan Pilih</option>
						<c:forEach var="item" items="${list}">
							<option value="${item.id}">${item.namaKategori}</option>
						</c:forEach>
				</select>
			</div>					
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Nama Genre</label>
			<div class="col-md-6">
				<input type="text" id="namaGenre" name="namaGenre"
					class="form-control">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Keterangan</label>
			<div class="col-md-6">
				<input type="text" id="keterangan" name="keterangan"
					class="form-control">
			</div>
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>