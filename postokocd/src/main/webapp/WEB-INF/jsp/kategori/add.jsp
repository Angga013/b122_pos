<form id="form-kategori" action="save" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control"
			value="insert">
		<div class="form-group">
			<label class="control-label col-md-2">ID</label>
			<div class="col-md-6">
				<input type="text" id="id" name="id" class="form-control">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Kategori</label>
			<div class="col-md-6">
				<input type="text" id="namaKategori" name="namaKategori"
					class="form-control">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Keterangan</label>
			<div class="col-md-6">
				<input type="text" id="keterangan" name="keterangan"
					class="form-control">
			</div>
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>