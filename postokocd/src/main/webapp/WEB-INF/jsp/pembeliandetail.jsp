<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">Detail Pembelian</h3>
		<div class="box-tools">
			<button type="button" id="btn-add" class="btn btn-primary pull-right"><i class="fa fa-plus"></i>Add Detail Pembelian</button>
			
		</div>
	</div>
	<div class="box-body">
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>ID Pembelian</th>
					<th>ID CD</th>
					<th>Harga Satuan</th>
					<th>Jumlah Beli</th>
					<th>Sub Total</th>
				</tr>
			</thead>
			<tbody id="list-data">
				
			</tbody>
		</table>
	</div>
</div>

<!-- Modal -->
<div id="modal-input" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form Detail Pembelian</h4>
			</div>
			<div class="modal-body">
			
			</div>
		</div>
	</div>
</div>

<script>
function loadData(){
	$.ajax({
		url:'pembelianDetail/list.html',
		type:'get',
		dataType:'html',
		success:function(data){
			$("#list-data").html(data);
		}
	});
}
	
	function loadCd(){
		$.ajax({
			url			: 'ajax/getCd.json',
			type		: 'get',
			dataType	: 'json',
			success		: function(data){
				$("#modal-input #cdId").empty();
				$("#modal-input #cdId").append('<option value="">Pilih CD</option>');
				$.each(data.result,function(index,item){
					$("#modal-input #cdId").append('<option value="'+ item.id +'">'+ item.namaCd +'</option>');
				});
			}
		});
	}
	loadData();
	
	$(document).ready(function(){
		
		$("#btn-add").on("click",function(){
			$.ajax({
				url:'pembelianDetail/add.html',
				type:'get',
				dataType:'html',
				success:function(data){
					$("#modal-input").find(".modal-body").html(data);
					loadCd();
					$("#modal-input").modal('show');
				}
			});
			
		});
		
		// button simpan di klik
		$("#modal-input").on("submit","#form-pembelian-detail",function(){
			$.ajax({
				url:'pembelianDetail/save.json',
				type:'post',
				data:$(this).serialize(),
				dataType:'json',
				success:function(data){
					if(data.result=="berhasil"){
						$("#modal-input").modal('hide');
						loadData();
					}
					else{
						$("#modal-input").modal('show');
					}
				}
			});
			return false;
		});
		
		// button edit di klik
		$("#modal-input").on("click",".btn-edit",function(){
			var vId = $(this).val();
			$.ajax({
				url			: 'pembelianDetail/edit.html',
				type		: 'get',
				data		: {id:vId},
				dataType	: 'html',
				success		: function(data){
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		
		// button delete di klik
		$("#modal-input").on("click",".btn-delete",function(){
			var vId=$(this).val();
			$.ajax({
				url			: 'pembelianDetail/delete.html',
				type		: 'get',
				data		: {id:vId},
				dataType	: 'html',
				success		: function(data){
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
	});
</script>