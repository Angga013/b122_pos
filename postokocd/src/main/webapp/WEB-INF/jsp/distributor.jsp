<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">Data Distributor</h3>
		<div class="box-tools">
			<button type="button" id="btn-add" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add Distributor</button>
		</div>
	</div>
	<div class="box-body">
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nama Distributor</th>
					<th>Alamat</th>
					<th>Kecamatan</th>
					<th>Kabupaten/Kota</th>
					<th>Provinsi</th>
					<th>No. Telepon</th>
					<th>No. Fax</th>
					<th>Email</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data">
			
			</tbody>
		</table>
	</div>
</div>

<!-- Modal -->
<div id="modal-input" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form Distributor</h4>
			</div>
			<div class="modal-body">
			
			</div>
		</div>
	</div>
</div>

<script>
	function loadData(){
		$.ajax({
			url:'distributor/list.html',
			type:'get',
			dataType:'html',
			success:function(data){
				$("#list-data").html(data);
			}
		});
	}
	
	function loadProvinsi(){
		$.ajax({
			url:'ajax/getProvinsi.json',
			type:'get',
			dataType:'json',
			success:function(data){
				$("#modal-input #provinsiId").empty();
				$("#modal-input #provinsiId").append('<option value="">Pilih Provinsi</option>');
				$.each(data.result,function(index,item){
					$("#modal-input #provinsiId").append('<option value="'+ item.id +'">'+ item.namaProvinsi +'</option>');
				});
			}
		});
	}
	
	function loadKota(id){
		$.ajax({
			url:'ajax/getKota.json',
			type:'get',
			data:{id:id},
			dataType:'json',
			success:function(data){
				$("#modal-input #kotaId").empty();
				$("#modal-input #kotaId").append('<option value="">Pilih Kota</option>');
				$.each(data.result,function(index,item){
					$("#modal-input #kotaId").append('<option value="'+ item.id +'">'+ item.namaKota +'</option>');
				});
			}
		});
	}
	
	function loadKecamatan(id){
		$.ajax({
			url:'ajax/getKecamatan.json',
			type:'get',
			data:{id:id},
			dataType:'json',
			success:function(data){
				$("#modal-input #kecamatanId").empty();
				$("#modal-input #kecamatanId").append('<option value="">Pilih Kecamatan</option>');
				$.each(data.result,function(index,item){
					$("#modal-input #kecamatanId").append('<option value="'+ item.id +'">'+ item.namaKecamatan +'</option>');
				});
			}
		});
	}
	loadData();
	
	$(document).ready(function(){
		
		$("#btn-add").on("click",function(){
			$.ajax({
				url:'distributor/add.html',
				type:'get',
				dataType:'html',
				success:function(data){
					$("#modal-input").find(".modal-body").html(data);
					loadProvinsi();
					$("#modal-input").modal('show');
				}
			});
			
		});
		
		$("#modal-input").on("change","#provinsiId",function(){
			var id = $(this).val();
			$("#modal-input #kotaId").empty();
			$("#modal-input #kotaId").append('<option value="">Pilih Kota</option>');
			$("#modal-input #kecamatanId").empty();
			$("#modal-input #kecamatanId").append('<option value="">Pilih Kecamatan</option>');
			loadKota(id);	
		});
		
		$("#modal-input").on("change","#kotaId",function(){
			var id = $(this).val();
			loadKecamatan(id);			
		});	
		
		$("#modal-input").on("submit","#form-distributor",function(){
			$.ajax({
				url:'distributor/save.json',
				type:'post',
				data:$(this).serialize(),
				dataType:'json',
				success:function(data){
					if(data.result=="berhasil"){
						$("#modal-input").modal('hide');
						loadData();
					}
					else{
						$("#modal-input").modal('show');
					}
				}
			});
			return false;
		});
		
		// button edit di klik
		$("#list-data").on("click",".btn-edit",function(){
			var vId = $(this).val();
			$.ajax({
				url:'distributor/edit.html',
				type:'get',
				data:{ id:vId },
				dataType:'html',
				success:function(data){
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		
		// button delete
		$("#list-data").on("click",".btn-delete",function(){
			var vId = $(this).val();
			$.ajax({
				url:'distributor/delete.html',
				type:'get',
				data:{ id:vId },
				dataType:'html',
				success:function(data){
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
	});
</script>